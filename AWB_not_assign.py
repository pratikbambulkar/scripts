import csv
import mysql.connector as sql  # to connect with mysql server pip install mysql-connector
import os

os.remove("output.csv")
os.remove("bag_packed_awb.csv")
os.remove("awb.csv")


# connecting avis db
db_connection_avis = sql.connect(
    host="jioecomm-avis-rr-1.cgszbyyjeamy.ap-south-1.rds.amazonaws.com",
    user="fynd_avis",
    database="avis",
    password="avis_readWrite_retail!2017",
)

db_connection_avis.autocommit = False

query = f"""select 
  stat.shipment_id, 
  stat.status, 
  stat.created_at, 
  json_extract(
    ship.meta, "$.shipment_dimensions.to_pincode"
  ) as Customer_Pincode, 
  json_extract(
    ship.meta, "$.shipment_dimensions.from_pincode"
  ) as Store_Pincode 
from 
  shipment as ship 
  inner join shipment_status as stat on ship.current_shipment_status = stat.id 
where 
  stat.status = 'bag_packed' 
  and ship.shipment_type = '3P' 
  and ship.is_active = 1 
  and stat.bag_list != '' 
  and ship.created_at between '2023-08-20 00:00:00' 
  and '2023-08-30 23:59:59';"""

cursor = db_connection_avis.cursor()
cursor.execute(query)
avis = cursor.fetchall()
avis_fp = open("bag_packed_awb.csv", "w+")
avis_my_file = csv.writer(avis_fp)
avis_my_file.writerow(["shipment_id", "status", "created_at", "customer_pincode", "store_pincode"])
avis_my_file.writerows(avis)
avis_fp.close()

# taking the shipments from the bag_packed_awb.csv
shipments_list = []
file = open("bag_packed_awb.csv")
csvreader = csv.reader(file)
for index, row in enumerate(csvreader):
    if index == 0:
        continue
    shipments_list.append(row[0])

#  connecting hedwig db
conn = sql.connect(
    user="hedwig",
    password="fyndDbMySQL#kjhkjh",
    host="jioecomm-hedwig-rr-1.cgszbyyjeamy.ap-south-1.rds.amazonaws.com",
    database="hedwig",
)

fp = open("awb.csv", "a")
myFile = csv.writer(fp)
myFile.writerow(["shipment_id", "dp", "raw_response"])

# Setting auto commit false
conn.autocommit = False

# Creating a cursor object using the cursor() method
cursor = conn.cursor()

# Retrieving data
# Can be added any query here to extract whatever data is required
for index, shipment in enumerate(shipments_list):
    query = f"""select rl.shipment_id,acc.name as Delivery_Partner,rl.raw_response 
    from request_logger as rl INNER JOIN account as acc ON rl.account_id = acc.id where rl.shipment_id = '{shipment}' 
    order by rl.id desc limit 1; """
    cursor.execute(query)
    avs = cursor.fetchall()

    # If we don't want our data in csv file we could just simply print result
    # print(avs)

    # this way we can print data in csv format.
    if index == 0:
        myFile.writerows(avs)

    else:
        fp = open("awb.csv", "a")
        myFile = csv.writer(fp)
        myFile.writerows(avs)
        fp.close()


def vlookup(csv_file1, csv_file2, key_column, lookup_columns, output_file):
    # Read the first CSV file and create a dictionary with the key column as the key
    data_dict = {}
    with open(csv_file1, 'r') as file1:
        reader1 = csv.DictReader(file1)
        for row in reader1:
            key = row[key_column]
            data_dict[key] = row

    # Read the second CSV file and append the lookup columns from the first file
    result = []
    with open(csv_file2, 'r') as file2:
        reader2 = csv.DictReader(file2)
        for row in reader2:
            key = row[key_column]
            lookup_values = data_dict.get(key)
            for col in lookup_columns:
                row[col] = lookup_values.get(col)

            result.append(row)

    # Write the updated rows to the output CSV file
    with open(output_file, 'w', newline='') as outfile:
        writer = csv.DictWriter(outfile, fieldnames=reader2.fieldnames + lookup_columns)
        writer.writeheader()
        writer.writerows(result)


# Example usage
csv_file1 = 'bag_packed_awb.csv'  # Replace with the path to your first CSV file
csv_file2 = 'awb.csv'  # Replace with the path to your second CSV file
key_column = 'shipment_id'  # Replace with the column name used as the key for matching
lookup_columns = ['customer_pincode', 'store_pincode', 'created_at']
# Replace with the column name to be looked up and appended

output_file = 'output.csv'  # Replace with the desired output file path

vlookup(csv_file1, csv_file2, key_column, lookup_columns, output_file)

# while running the script please install mysql.connector and remove all the three files from your pods
# bag_packed_awb.csv, awb.csv, output.csv
# while running the script please pass the date range in avis query
