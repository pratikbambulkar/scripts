import pip


def install(package):
    if hasattr(pip, 'main'):
        pip.main(['install', package])
    else:
        pip._internal.main(['install', package])


if __name__ == '__main__':
    install('csv')


from pymongo import MongoClient
from config import current_config
import json
import csv



connection_uri = current_config.MONGO_URI
client = MongoClient(connection_uri)
db = client.computron


with open('test_shipments.txt', 'r') as df:
    shipment_id = list(csv.reader(df, delimiter='\t'))
    input_files = [i[0] for i in shipment_id]

# print(input_files)


with open('comp_final_shipment_dp_meta_info' + '.csv', 'a') as fp:
    myFile = csv.writer(fp)
    myFile.writerow(['shipment_id', 'updated_at','assigned_dp_id', 'shipment_status', 'account_options_id_list', 'assigned_dp_name', "account_info_meta", 'account_options_meta'])
    fp.close()



for input_file in input_files:
    shipments_data = db.shipments.find({'shipment_id': input_file})

    for data in shipments_data:
        print(data)
        break
        # assigned_dp_id = data['affiliate_details']['shipment_meta']['logistics_meta']['account_info']['internal_account_id']
        # assigned_dp_name = data['affiliate_details']['shipment_meta']['logistics_meta']['account_info']['name']
        #
        # account_info_meta = data['affiliate_details']['shipment_meta']['logistics_meta']['account_info']
        # account_options_meta = data['affiliate_details']['shipment_meta']['logistics_meta']['account_options']
        #
        # shipment_status = data['shipment_status']['status']
        #
        # updated_at = str(data['shipment_status']['created_at'])
        #
        # account_options_id_list = [x['internal_account_id'] for x in data['affiliate_details']['shipment_meta']['logistics_meta']['account_options']]

        # with open('comp_final_shipment_dp_meta_info' + '.csv', 'a') as fp:
        #     myFile = csv.writer(fp)
        #     myFile.writerow([input_file, updated_at,assigned_dp_id, shipment_status, str(account_options_id_list),assigned_dp_name, account_info_meta, account_options_meta])
        #     fp.close()


print('done')




