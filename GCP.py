from google.cloud import bigquery
import pandas as pd
client = bigquery.Client.from_service_account_json('/home/pratikbambulkar/keys.json')

query = """SELECT primary_identifier , secondary_identifier ,created_on , uid, state, status, state_index FROM `jiomart.jioecomm_main_dataset_v1.hogwarts_task` where integration_id =5 and state_index =1 and state ='placed' and status ='completed' and DATE(created_on)  between '2022-10-01' and '2022-11-10';"""

data_df = pd.read_gbq(query, project_id='jiomart', dialect='standard')

# print(data_df.head(1))

data_df.to_csv('hogwarts_placed_data.csv')