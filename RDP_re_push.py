import json
import psycopg2.extras
import copy

conn = psycopg2.connect(
    database='hogwarts', user='adhoc_user', password='CW4k75c24vc23D', host="jioecomm-hogwarts-rr-1.cgszbyyjeamy.ap"
                                                                            "-south-1.rds.amazonaws.com", port='5432')

# Setting auto commit false
conn.autocommit = False

# Creating a cursor object using the cursor() method
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)


from models import Integration, Task, TaskEvents
from connections import Backend
import requests

session = Backend().get_session()


# task_uid = ""
# data = json.loads("""{}""")
def update(task_uid, datas):
    data = json.loads(f'''{datas}''')
    # print(task_uid)
    task_obj = session.query(Task).filter_by(uid=task_uid).one()
    task_obj.data = data
    session.commit()

    resp = requests.post('https://api.jioecomm.com/hogwarts/api/v1/manual-retry-api',
                         headers={"Content-Type": "application/json"},
                         data=json.dumps({"uid": [task_uid]}))
    print(resp, resp.text)


def bulk_update(uid, old, shipment):
    try:

        parsed_data = json.loads(old)

        # print(parsed_data)

        def update_package_attributes(target_data, source_data):
            target_data["quantity"] = source_data["quantity"]
            target_data["article_id"] = source_data["article_id"]
            target_data["additional_details"] = {
                "unit_width": source_data["unit_width"],
                "unit_height": source_data["unit_height"],
                "unit_length": source_data["unit_length"],
                "weight_unit": source_data["weight_unit"],
                "dimensions_unit": source_data["dimensions_unit"],
                "unit_gross_weight": source_data["unit_gross_weight"],
                "total_buffer_weight": source_data["total_buffer_weight"]
            }

        new_payload = copy.deepcopy(parsed_data)

        package_attributes = \
            parsed_data["_original_data"]["shipment"]["meta"]["shipment_dimensions"]["package_attributes"]
        for index, row in enumerate(package_attributes):
            # print(row)
            inside_parsed_data = \
            parsed_data["_original_data"]["shipment"]["meta"]["shipment_dimensions"]["package_attributes"][index]

            # shipment key updated
            update_package_attributes(
                new_payload["_original_data"]["shipment"]["meta"]["shipment_dimensions"]["package_attributes"][index],
                inside_parsed_data)

            keys = ['unit_width', 'unit_height', 'unit_length', 'weight_unit', 'dimensions_unit', 'unit_gross_weight',
                    'total_buffer_weight']

            for key in keys:
                new_payload["_original_data"]["shipment"]["meta"]["shipment_dimensions"]["package_attributes"][
                    index].pop(key)

        # # affiliate_details updated
        # update_package_attributes(
        #     new_payload["_original_data"]["affiliate_details"]["shipment_meta"]["shipment_dimensions"][
        #         "package_attributes"][0], inside_parsed_data)

        json_string = json.dumps(new_payload)
        # print(uid)
        # print(json_string)
        update(uid, json_string)


    except Exception as e:
        print(e)
        print(shipment)


shipments_list = ['16948454847591793630J']

for index, shipment in enumerate(shipments_list):
    query_one = f'''select uid, data from task where integration_id = 5 and 
    primary_identifier = '{shipment}' and state = 'return_dp_assigned' and state_index = 0; '''

    cursor.execute(query_one)
    hog_one = cursor.fetchall()
    old_json = json.dumps(hog_one[0][1])
    uid = hog_one[0][0]
    # print(uid)
    # print(old_json)

    bulk_update(uid, old_json, shipment)
