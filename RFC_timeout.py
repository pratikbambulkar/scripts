import json
import psycopg2.extras

conn = psycopg2.connect(
    database='hogwarts', user='adhoc_user', password='CW4k75c24vc23D', host="jioecomm-hogwarts-rr-1.cgszbyyjeamy.ap"
                                                                            "-south-1.rds.amazonaws.com", port='5432')

# Setting auto commit false
conn.autocommit = False

# Creating a cursor object using the cursor() method
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)


from models import Integration, Task, TaskEvents
from connections import Backend
import requests
session = Backend().get_session()
# task_uid = ""
# data = json.loads("""{}""")
def update(task_uid, datas):
    data = json.loads(f'''{datas}''')
    print(data)
    print(task_uid)
    task_obj = session.query(Task).filter_by(uid=task_uid).one()
    task_obj.data = data
    session.commit()

    resp = requests.post('https://api.jioecomm.com/hogwarts/api/v1/manual-retry-api', headers={"Content-Type": "application/json"},
                         data=json.dumps({"uid": [task_uid]}))
    print(resp, resp.text)





def bulk_update(old, IB_response, uid):
    # Load JSON data
    parsed_data = json.loads(old)
    # print(parsed_data)
    parsed_IB_data = json.loads(IB_response)
    # # print(parsed_IB_data)
    # print(parsed_data["_data"]['orderInvoiceList'][0]['invoiceData'])
    parsed_data["_data"]['orderInvoiceList'][0]['invoiceData'] = parsed_IB_data['result']['invoices'][0]['invoice_data']

    parsed_data["_data"]['orderInvoiceList'][0]['invoiceNumber'] = parsed_IB_data['result']['invoices'][0]['invoice_number']

    parsed_data["invoice_data"] = parsed_IB_data['result']['invoices']

    json_string = json.dumps(parsed_data)
    # print(uid)
    # print(json_string)
    update(uid, json_string)


shipments_list = ['16921603994321182795J']

for index, shipment in enumerate(shipments_list):
    query_one = f'''select tk.data from task tk join task_events te ON te.task_id = tk.uid where integration_id = 5 and 
    primary_identifier = '{shipment}' and state = 'bag_confirmed' and state_index = 2 limit 1; '''

    query_two = f'''select te.response_text from task tk join task_events te ON te.task_id = tk.uid where integration_id = 5 
    and primary_identifier = '{shipment}' and state = 'bag_confirmed' and state_index = 1 limit 1; '''

    query_three = f'''select uid from task where integration_id = 5 and primary_identifier = '{shipment}' 
    and state = 'bag_confirmed' and state_index = 2;'''


    cursor.execute(query_one)
    hog_one = cursor.fetchall()
    old_json = json.dumps(hog_one[0][0])
    # print(old_json)

    cursor.execute(query_two)
    hog_two = cursor.fetchall()
    IB_response = hog_two[0][0]
    # print(IB_response)

    cursor.execute(query_three)
    uid = cursor.fetchall()[0][0]
    # print(uid)

    bulk_update(old_json, IB_response, uid)


