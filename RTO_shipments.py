from google.cloud import bigquery
import pandas as pd
import csv
client = bigquery.Client.from_service_account_json('/home/pratikbambulkar/keys.json')


def order_ids(data):
    return tuple(x[0] for x in data)


with open('order_ids.txt', 'r') as df:
    shipment_ids = list(csv.reader(df, delimiter='\t'))

shipment_id = order_ids(shipment_ids)

#
query = f"""SELECT shipment_id, status, created_at FROM `jiomart.jioecomm_main_dataset_v1.avis_shipment_status` WHERE DATE(created_at) < "2023-01-08" and shipment_id in {shipment_id} and status in ('rto_initiated')"""

data_df = pd.read_gbq(query, project_id='jiomart', dialect='standard')

data_df.to_csv('RTO_shipments.csv')




