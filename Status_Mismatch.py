import pip


def install(package):
    if hasattr(pip, 'main'):
        pip.main(['install', package])
    else:
        pip._internal.main(['install', package])


if __name__ == '__main__':
    install('pandas')
    install('csv')

import json
import csv
import requests
from datetime import datetime
import pandas as pd


def isNaN(num):
    return num != num


def bag_picked_api_push(awb_no, shipment_id, payment_url):
    url = "https://ril-prod-sellow-vpce.ril.smebazaar.ooo/pvt/api/shipments/update_shipping_status.json"

    print(payment_url)
    if isNaN(payment_url):
        payload = json.dumps({
            "tracking_code": str(awb_no),
            "shipping_status": "pick_up_confirmed",
            "shipment_id": shipment_id,
            "waybill": [
                str(awb_no)
            ],
            "shipping_agent_code": 'delhivery_jio',
            "payment_qr_code_url": "",
            "to_pincode": "None"
        })
    else:
        payload = json.dumps({
            "tracking_code": str(awb_no),
            "shipping_status": "pick_up_confirmed",
            "shipment_id": shipment_id,
            "waybill": [
                str(awb_no)
            ],
            "shipping_agent_code": 'delhivery_jio',
            "payment_qr_code_url": payment_url.replace('\"', ""),
            "to_pincode": "None"
        })

    headers = {
        'Testing': 'e1e50803314fade1dd8a930cd45bf3ce',
        'Authorization': 'Bearer 6c14f2c3a19890c2e221ea03905d5051c99136b6eafd65f004febed79b552d67',
        'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    print(payload)

    print(
        response.status_code
    )

    with open("bag_pick_pushed_vipul.csv", 'a') as cf:
        content_file = csv.writer(cf)
        content_file.writerow(
            [str(shipment_id), payload, str(response.json()), str(response.status_code), str(response.text),
             str(datetime.now())])
        cf.close()


with open("IB_bag_pushed_logs.csv", 'a') as cf:
    content_file = csv.writer(cf)
    content_file.writerow(["shipment_id", "Payload", "Response_text", "Status code", "Response_text", "date_time"])
    cf.close()

bag_picked_payload_data = pd.read_csv('bag_picked_push.csv')


for i, j, k in zip(bag_picked_payload_data['shipment_id'],
                   bag_picked_payload_data['awb_no'],
                   bag_picked_payload_data['payment_url']):

    bag_picked_api_push(awb_no=j, shipment_id=i, payment_url=k)

print('done')
