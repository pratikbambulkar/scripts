import pip

def install(package):
    if hasattr(pip, 'main'):
        pip.main(['install', package])
    else:
        pip._internal.main(['install', package])


if __name__ == '__main__':
    install('pandas')
    install('csv')

import json
import csv
import requests
from datetime import datetime
import pandas as pd

def bag_picked_api_push(awb_no, shipment_id, idempotency_key):
    url = "https://rriplb.ril.com/retail/3P/OrderDispatch/v1/processRequest"

    payload = json.dumps({
        "shipping_agent_code": "ecomm",
        "waybill": [
            str(awb_no)
        ],
        "shipping_status": "pick_up_confirmed",
        "picked_at": "None",
        "meta": {
            "time_stamp": "None",
            "raw_lsp_status": "None",
            "location": "None"
        },
        "tracking_code": str(awb_no),
        "shipment_id": shipment_id
    })

    headers = {
        'Recipient': 'ADDVERB',
        'Content-Type': 'application/json',
        'instanceCode': 'SKAB',
        'Authorization': 'Basic RllORFVTRVI6UGQjUiFMJEZ5bmQ=',
        'idempotency-key': str(idempotency_key)
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    print(response.text)

    print(response.status_code)

    with open("bag_picked_logs.csv", 'a') as file:
        content_files = csv.writer(file)
        content_files.writerow(
            [str(shipment_id), payload, str(response.json()), str(response.status_code), str(response.text),
             str(datetime.now())])
        file.close()


with open("bag_picked_logs.csv", 'a') as cf:
    content_file = csv.writer(cf)
    content_file.writerow(["shipment_id", "Payload", "Response_text", "Status code", "Response_text", "date_time"])
    cf.close()

bag_picked_payload_data = pd.read_csv('bag_picked_payload.csv')

for i, j, k in zip(bag_picked_payload_data['shipment_id'],
                   bag_picked_payload_data['awb_no'],
                   bag_picked_payload_data['idempotency_key']):
    bag_picked_api_push(shipment_id=i, awb_no=j, idempotency_key=k)

print('done')
