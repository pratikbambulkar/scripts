import json
import psycopg2.extras

conn = psycopg2.connect(
    database='hogwarts', user='adhoc_user', password='CW4k75c24vc23D', host="jioecomm-hogwarts-rr-1.cgszbyyjeamy.ap"
                                                                            "-south-1.rds.amazonaws.com", port='5432')

# Setting auto commit false
conn.autocommit = False

# Creating a cursor object using the cursor() method
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)


from models import Integration, Task, TaskEvents
from connections import Backend
import requests
session = Backend().get_session()
# task_uid = ""
# data = json.loads("""{}""")
def update(task_uid, datas):
    data = json.loads(f'''{datas}''')
    print(task_uid)
    task_obj = session.query(Task).filter_by(uid=task_uid).one()
    task_obj.data = data
    session.commit()

    resp = requests.post('https://api.jioecomm.com/hogwarts/api/v1/manual-retry-api', headers={"Content-Type": "application/json"},
                         data=json.dumps({"uid": [task_uid]}))
    print(resp, resp.text)



def bulk_update(old, instance_code, IB_data, invoice_data, uid):
    # Load JSON data
    parsed_data = json.loads(old)
    # print(parsed_data)
    parsed_IB_data = json.loads(IB_data)
    # print(parsed_IB_data)

    new_payload = {"_data": parsed_data["_data"],
                   "_meta": parsed_data["_meta"],
                   "play_action": None,
                   "_auth_config": None,
                   "invoice_data": [invoice_data],
                   "_original_data": parsed_data["_original_data"],
                   "fynd_store_type": "RFC",
                   "_request_headers": {
                       "Recipient": "ADDVERB",
                       "Content-Type": "application/json",
                       "instanceCode": instance_code,
                       "Authorization": "Basic RllORFVTRVI6UGQjUiFMJEZ5bmQ=",
                       "idempotency-key": "c2978049-6b52-4f73-a73d-ca4257483412"
                   },
                   "infibeam_invoice_data": parsed_IB_data,
                   "_access_token_redis_key": "ib_auth_token"

                   }

    json_string = json.dumps(new_payload)
    # print(uid)
    # print(json_string)
    update(uid, json_string)


shipments_list = ['16951086560381030572J']

for index, shipment in enumerate(shipments_list):
    query_one = f'''select tk.data from task tk join task_events te ON te.task_id = tk.uid where integration_id = 5 and 
    primary_identifier = '{shipment}' and state = 'bag_confirmed' and state_index = 2 limit 1; '''

    query_two = f'''select te.payload from task tk join task_events te ON te.task_id = tk.uid where integration_id = 5 
    and primary_identifier = '{shipment}' and state = 'bag_confirmed' and state_index = 1 limit 1; '''

    query_three = f'''select uid from task where integration_id = 5 and primary_identifier = '{shipment}' 
    and state = 'bag_confirmed' and state_index = 2;'''

    query_four = f'''select distinct te.headers ->> 'instanceCode' from task tk join task_events te ON te.task_id = 
    tk.uid where integration_id = 5 and tk.state='placed' and tk.state_index=1 and 
    tk.primary_identifier= '{shipment}';'''

    query_five = f'''select  to_jsonb(te.response_text :: jsonb) -> 'result' -> 'invoices' -> 0 from task tk join 
    task_events te ON te.task_id = tk.uid where integration_id = 5 and tk.state='bag_confirmed' and 
    tk.status='completed' and tk.state_index=1 and tk.primary_identifier = '{shipment}' limit 1 '''

    cursor.execute(query_one)
    hog_one = cursor.fetchall()
    old_json = json.dumps(hog_one[0][0])
    # print(old_json)

    cursor.execute(query_two)
    hog_two = cursor.fetchall()
    IB_data = str(hog_two[0][0]).replace("'", '"')
    # print(IB_data)

    cursor.execute(query_three)
    uid = cursor.fetchall()[0][0]
    # print(uid)

    cursor.execute(query_four)
    instance_code = cursor.fetchall()[0][0]
    # print(instance_code)

    cursor.execute(query_five)
    invoice_data = cursor.fetchall()[0][0]
    # print(invoice_data)

    bulk_update(old_json, instance_code, IB_data, invoice_data, uid)
