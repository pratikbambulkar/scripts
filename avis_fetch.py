import pip
import mysql.connector as sql
import csv

def install(package):
    if hasattr(pip, 'main'):
        pip.main(['install', package])
    else:
        pip._internal.main(['install', package])


if __name__ == '__main__':
     install('csv')
     install('mysql-connector')


db_connection_avis = sql.connect(host='jioecomm-avis-rr-1.cgszbyyjeamy.ap-south-1.rds.amazonaws.com', user='fynd_avis',
                                 database='avis', password='avis_readWrite_retail!2017')


db_connection_avis.autocommit = False


def order_ids(data):
    return tuple(x[0] for x in data)


def sql_data_fetch_avis(query):
    cursor = db_connection_avis.cursor()
    cursor.execute(query)
    order_data, col = cursor.fetchall(), [i[0] for i in cursor.description]
    fp = open('order_ids_data.csv', 'w')
    myFile = csv.writer(fp)
    myFile.writerow(col)
    myFile.writerows(order_data)
    fp.close()
    print("Done")


with open('order_ids.txt', 'r') as df:
    shipment_ids = list(csv.reader(df, delimiter='\t'))

shipment_id = order_ids(shipment_ids)

query_status_orders = f""" select shipment_id, status, created_at from shipment_status where shipment_id in {shipment_id} and status in ('cancelled_customer','cancelled_operations','return_initiated','refund_without_return','cancelled_seller')"""

sql_data_fetch_avis(query_status_orders)


# select shipment_id, status from shipment_status where shipment_id in {shipment_id} and status in ('cancelled_customer','cancelled_operations','return_initiated','refund_without_return','cancelled_seller')

# select previous_shipment_id, id from shipment where previous_shipment_id in {shipment_id}