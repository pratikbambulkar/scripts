# 1. get bag_confirmed state for infibeam( state_index : 1) and addverb (2)
# 2. take response of infibeam from task_event ("result" node)
# 3. copy it to addverb payload ( from task)
# 4. update data, task_uid (addverb uid from task) values in the following script, run

import json
from models import Integration, Task, TaskEvents
from connections import Backend
import requests
session = Backend().get_session()
task_uid = ""
data = json.loads("""{}""")
def update(task_uid, data):
    task_obj = session.query(Task).filter_by(uid=task_uid).one()
    task_obj.data = data
    session.commit()

    resp = requests.post('https://api.jioecomm.com/hogwarts/api/v1/manual-retry-api', headers={"Content-Type": "application/json"},
                         data=json.dumps({"uid": [task_uid]}))
    print(resp, resp.text)