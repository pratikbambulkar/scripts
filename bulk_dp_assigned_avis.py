import requests
import json

url = "https://api.jioecomm.com/__jiomart/oms/fulfillment/api/v1/oms/manual-place-shipment/"

shipment_id = ['16919996028201958758J']

for shipment in shipment_id:
    payload = json.dumps({
      "shipment_ids": [
        shipment
      ]
    })
    headers = {
      'cache-control': 'no-cache',
      'Content-Type': 'application/json',
      'Cookie': 'f.session=s%3A6marTd6hCL4DD7toEzq67CszqzHfAhl-.HXVaGY%2FFucHaDVklOorb76E6%2B2pLzgZR977GNCw16Kk'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    print(response.text)


