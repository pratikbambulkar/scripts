import requests
import json

uid_list = ['abb21e6c-ad4e-42ce-a1d5-fc3cf7f82bc8']

for uid in uid_list:
  url = "https://api.jioecomm.com/hogwarts/api/v1/manual-retry-api"

  payload = json.dumps({
    "uid": [
      uid
    ]
  })
  headers = {
    'Content-Type': 'application/json'
  }

  response = requests.request("POST", url, headers=headers, data=payload)
  print(f"retry successful with {response.text} for uid: {uid} ")