import requests, string, csv, getpass, json, datetime

input_file_name = input('Please enter the Old Network file name: ')
url_domain = 'api.jiox2.de'


def access_token():
    url = "https://" + url_domain + "/__jiomart/logistics-internal/api/v1/auth/token/"
    payload = json.dumps({
        "grant_type": "client_credentials",
        "client_id": "JIOMART_TEST_CLIENT_ID_1",
        "client_secret": "TESTTOKEN"
    })
    headers = {
        'Content-Type': 'application/json'
    }
    response = requests.request("POST", url, headers=headers, data=payload)
    return (response.json()['access_token'])


auth = access_token()
auth = "Bearer " + auth + ""

url = "https://" + url_domain + "/__jiomart/logistics-internal/api/v1/onboarding/store/"

with open(input_file_name, 'r') as csvfile:
    input_file = list(csv.reader(csvfile, delimiter=','))

    for i in input_file:

        payload = json.dumps(
            {
                "name": "store|" + i[0],
                "display_name": i[1],
                "company_details": {
                    "business_country": "INDIA",
                    "business_country_code": "IN",
                    "company_name": "Reliance Retail Limited",
                    "company_id": "HBGCFH"
                },
                "address": {
                    "address1": "add",
                    "address2": "add",
                    "pincode": i[1],
                    "city": "city",
                    "country": "India",
                    "state": "state",
                    "latitude": "19.2850556",
                    "longitude": "72.867689"
                },
                "contact_numbers": [
                    {
                        "number": "8879012105",
                        "country_code": 91
                    }
                ],
                "notification_emails": [
                    "pujadhabale@gofynd.com"
                ],
                "code": i[0],
                "created_on": str(datetime.datetime.now()),
                "modified_on": str(datetime.datetime.now()),
                "batch_id": str(getpass.getuser()) + '_' + str(datetime.datetime.now()).strip().replace(' ', '_'),
                "_custom_json": {
                    "hub_code": "null",
                    "instance_code": "null",
                    "is_active": True,
                    "jio_store_id": i[0],
                    "jio_store_type": "FC",
                    "fynd_store_type": "BEAUTY",
                    "verticals": [

                    ],

                    "jio_sale_point": "RRL_FC",
                    "taxpayer_full_name": "abc"

                },
                "documents": [],
                "holidays": [],
                "timing": [
                    {
                        "weekday": "monday",
                        "open": True,
                        "opening": {
                            "hour": 10,
                            "minute": 0
                        },
                        "closing": {
                            "hour": 19,
                            "minute": 0
                        }
                    },
                    {
                        "weekday": "tuesday",
                        "open": True,
                        "opening": {
                            "hour": 10,
                            "minute": 0
                        },
                        "closing": {
                            "hour": 19,
                            "minute": 0
                        }
                    },
                    {
                        "weekday": "wednesday",
                        "open": True,
                        "opening": {
                            "hour": 10,
                            "minute": 0
                        },
                        "closing": {
                            "hour": 19,
                            "minute": 0
                        }
                    },
                    {
                        "weekday": "thursday",
                        "open": True,
                        "opening": {
                            "hour": 10,
                            "minute": 0
                        },
                        "closing": {
                            "hour": 19,
                            "minute": 0
                        }
                    },
                    {
                        "weekday": "friday",
                        "open": True,
                        "opening": {
                            "hour": 10,
                            "minute": 0
                        },
                        "closing": {
                            "hour": 19,
                            "minute": 0
                        }
                    },
                    {
                        "weekday": "saturday",
                        "open": True,
                        "opening": {
                            "hour": 10,
                            "minute": 0
                        },
                        "closing": {
                            "hour": 19,
                            "minute": 0
                        }
                    },
                    {
                        "weekday": "sunday",
                        "open": True,
                        "opening": {
                            "hour": 10,
                            "minute": 0
                        },
                        "closing": {
                            "hour": 19,
                            "minute": 0
                        }
                    }
                ]
            }

        )
        # print(payload)

        headers = {
            'Authorization': auth,
            'Content-Type': 'application/json'
        }

        response = requests.request("PATCH", url, headers=headers, data=payload)

        # print(response)
