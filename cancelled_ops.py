import subprocess

import openpyxl

package_names = ['pandas', 'mqsql.connector', 'openpyxl']

for package_name in package_names:
    subprocess.run(['pip', 'install', package_name])

import csv
import pandas as pd
import mysql.connector as sql  # to connect with mysql server pip install mysql-connector
from openpyxl import Workbook


# connecting avis db
db_connection_avis = sql.connect(
    host="jioecomm-avis-rr-1.cgszbyyjeamy.ap-south-1.rds.amazonaws.com",
    user="fynd_avis",
    database="avis",
    password="avis_readWrite_retail!2017",
)

cursor = db_connection_avis.cursor()

shipments = pd.read_excel('shipments.xlsx')
# print(shipments)
# print(shipments.columns[0])
shipment_ids = tuple(shipments['Shipment_Id'])
print(shipment_ids)

# Execute the MySQL query
query = f'''select previous_shipment_id,id from shipment where id in (select id from shipment where previous_shipment_id IN {shipment_ids});'''
cursor.execute(query)

# Fetch all the rows from the query result
query_result = cursor.fetchall()

# Load the existing Excel file
workbook = openpyxl.load_workbook('shipments.xlsx')

# Create a new sheet
new_sheet = workbook.create_sheet(title='New Sheet')

# Populate the new sheet with query data
new_sheet.append(cursor.column_names)
for row in query_result:
    new_sheet.append(row)

# Save the modified Excel file
workbook.save('shipments.xlsx')

# Close the cursor and connection
cursor.close()
db_connection_avis.close()








