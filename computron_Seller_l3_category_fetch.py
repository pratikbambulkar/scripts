#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 10 18:34:48 2022

@author: chirayukainya
"""
import os
import pandas as pd

computron_data = pd.DataFrame()
from pymongo import MongoClient

client = MongoClient(
    'mongodb://computron_readWrite:computron_readWrite!2018@10.0.16.13:27017,10.0.81.133:27017,10.0.16.175:27017/computron?replicaSet=re_computron2_set&readPreference=secondaryPreferred')
db = client.computron
collection = db.jio_products
# cursor = collection.find({"shipment_id": { '$in' : x}})
# cursor = collection.find({"shipment_id": shipment_id})

cursor = collection.find({}, {"classification": 1, "manufacturer": 1}, no_cursor_timeout=True)
computron_data = pd.DataFrame(list(cursor))


def source_name(x):
    # print(x)
    str_1 = ""
    if str(x) != 'nan':
        for i in range(len(x['category'])):
            if i == 0:
                str_1 = str_1 + x['category'][i]['source_name']
            else:
                str_1 = str_1 + " - " + x['category'][i]['source_name']

        return str_1
    else:
        return "NA"


os.chdir(r'/Users/chirayukainya/Desktop/Fynd/Projects/Seller_level_Category/Data/')

computron_data.to_csv('base_data.csv.gz', index=False, compression='gzip')

computron_data['source_name'] = computron_data['classification'].apply(lambda x: source_name(x))
computron_data['manufacturer_name'] = computron_data['manufacturer'].apply(
    lambda x: x['name'] if str(x) != 'nan' else 'NA')
computron_data.drop(['classification', 'manufacturer'], inplace=True, axis=1)

computron_data = computron_data[['source_name', 'manufacturer_name']]

computron_data.drop_duplicates(inplace=True, keep=False)

os.chdir(r'/Users/chirayukainya/Desktop/Fynd/Projects/Seller_level_Category/Data/')

computron_data.to_csv('Seller_level_category.csv', index=False)