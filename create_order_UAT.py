import requests
import json
import csv
from datetime import datetime

with open("create_order.csv", 'a') as cf:
    content_file = csv.writer(cf)
    content_file.writerow(
        ["Order_Id", "Payload", "Status_code", "Response", "Date_Time"])
    cf.close()


def create_order(order_id):
    url = "https://api.jiox3.de/__jiomart/oms/external/api/v1/morningstar/order/create/"
    null = None
    true = True
    false = False
    payload = json.dumps({
    "source": "jiomart",
    "shipments": [
      {
        "meta": {
          "shipment_timestamp": 1693899932
        },
        "promise": {
          "formatted": {
            "max": "09 Sep, Saturday",
            "min": "06 Sep, Wednesday"
          },
          "timestamp": {
            "max": 1694245532,
            "min": 1693986332
          }
        },
        "articles": [
          {
            "sku": "RVUWL2CSFA",
            "meta": {
              "is_hvi": false,
              "is_hazmat": false,
              "is_liquid": false,
              "pre_order": false,
              "is_fragile": false,
              "expiry_date": null,
              "delivery_type": "grab_and_go",
              "5G_sim_availed": false,
              "transport_mode": null,
              "assign_container": false,
              "lookup_inventory": true,
              "procurement_date": null,
              "assign_dp_from_sb": true,
              "country_of_origin": "NA",
              "is_dangerous_goods": false,
              "available_at_3p_kirana": false,
              "doorstep_finance_availed": false,
              "imei_validation_required": false
            },
            "prices": {
              "esp": 70.0,
              "mrp": 120.0,
              "meta": {

              },
              "amount_paid": 70.0,
              "cod_charges": 0,
              "coupon_json": {

              },
              "emi_discount": 0,
              "price_marked": 120.0,
              "refund_amount": 70.0,
              "promo_discount": 0,
              "coupon_discount": 0,
              "price_effective": 70.0,
              "delivery_charges": 0,
              "instant_discount": 0,
              "product_discount": 50.0,
              "margin_percentage": 0,
              "brand_coupon_discount": 0,
              "brand_calculated_amount": 70.0
            },
            "quantity": 1,
            "vertical": "GROCERIES",
            "image_url": "rvuwl2csfa/everyuth-naturals-walnut-exfoliating-scrub-50-g-front-view-orvuwl2csfa-p100005278-0-202305251147.jpg",
            "pm_prices": {
              "COD": 70.0
            },
            "article_id": "rvuwl2csfa",
            "food_stamp": 0,
            "is_tradein": false,
            "gift_article": 0,
            "product_name": "Everyuth Naturals Walnut Exfoliating Scrub 50 g",
            "weight_range": "",
            "article_group": [

            ],
            "home_delivery": 0,
            "ship_separate": false,
            "pm_prices_meta": {
              "COD": {
                "slug": "COD",
                "txn_date": "2023-09-05T13:15:40",
                "payment_id": "20",
                "payment_amt": 70.0,
                "payment_cart": null,
                "payment_desc": "COD",
                "bdcustomer_id": null,
                "order_inv_num": null,
                "order_app_code": "",
                "mode_of_payment": "COD",
                "payment_gateway_logo": null,
                "transaction_ref_number": str(order_id)
              }
            },
            "exchange_details": null,
            "processed_quantity": 1,
            "imei_validation_required": false
          }
        ],
        "vertical": "GROCERIES",
        "shipments": 1,
        "l2_vertical": "3P",
        "shipment_type": "3P",
        "container_meta": {
          "name": null,
          "count": 0,
          "assign_container": false,
          "ship_in_own_container_flag": false
        },
        "logistics_meta": {
          "dp_sort_key": "fm_priority",
          "account_info": {
            "name": "qwik_shipsy",
            "area_code": {
              "to_pincode": null,
              "from_pincode": null
            },
            "operations": "inter_city",
            "fm_priority": 6,
            "lm_priority": 6,
            "payment_mode": "all",
            "rvp_priority": 6,
            "transport_mode": "air",
            "assign_dp_from_sb": true,
            "external_account_id": "qwik_shipsy",
            "internal_account_id": "25"
          },
          "account_options": [
            {
              "name": "qwik_shipsy",
              "area_code": {
                "to_pincode": null,
                "from_pincode": null
              },
              "operations": "inter_city",
              "fm_priority": 6,
              "lm_priority": 6,
              "payment_mode": "all",
              "rvp_priority": 6,
              "transport_mode": "air",
              "assign_dp_from_sb": true,
              "external_account_id": "qwik_shipsy",
              "internal_account_id": "25"
            }
          ],
          "assign_dp_from_sb": true
        },
        "display_message": "Delivery Between 6th Sep to 9th Sep",
        "fulfillment_meta": {
          "meta": {
            "mid": "6833",
            "company_id": "ODJJQ1",
            "store_type": "3P"
          },
          "tenant_id": "1007",
          "tenant_ids": [
            "1007"
          ],
          "seller_name": "Srl limited",
          "fulfillment_id": "3PODJJQ1FC07",
          "jio_sale_point": "3P",
          "fulfillment_type": "SMART",
          "mapped_stockpoint": null
        },
        "delivery_slot_meta": null,
        "secondary_display_message": "Delivery Between 5th Sep to 7th Sep"
      }
    ],
    "mixed_cart": false,
    "order_prices": {
      "order_type": "COD",
      "cod_charges": 0,
      "gross_total": 120.0,
      "tax_details": {
        "gstin": null,
        "gst_legal_name": null
      },
      "final_amount": 70.0,
      "cashback_value": 0,
      "delivery_charges": 0.0,
      "total_order_discount": 50.0,
      "total_promo_discount": 0.0,
      "total_coupon_discount": 0.0,
      "total_product_discount": 50.0
    },
    "customer_data": {
      "id": 80,
      "pancard": "NA",
      "email_id": "pratikbambulkar429@gmail.com",
      "last_name": "Bambulkar",
      "first_name": "Pratik ",
      "membership_id": "9819862946",
      "mobile_number": "9819862946",
      "pancard_lastname": "",
      "pancard_firstname": "",
      "pancard_middlename": "",
      "loyalty_card_number": ""
    },
    "fynd_order_id": "FYMP64F6DCA401886526",
    "order_details": {
      "channel_id": "web",
      "extra_meta": {
        "is_lead": false,
        "whatsapp_notification": false
      },
      "raw_user_agent": {

      },
      "order_created_date": "2023-09-05T13:15:40",
      "payment_gateway_aggregator": "myjio"
    },
    "billing_address": {
      "lat": 18.900297285,
      "lon": 72.816089557,
      "pin": "400005",
      "area": "",
      "city": "Mumbai",
      "state": "Maharashtra",
      "sector": "Test Account",
      "street": "Test Account",
      "flat_no": "NA",
      "plot_no": "",
      "block_no": "",
      "floor_no": "NA",
      "tower_no": null,
      "addresstype": "home",
      "apartment_id": null,
      "society_name": "",
      "building_name": "NA",
      "building_type": "apartment",
      "addressee_name": "Pratik  Bambulkar",
      "billing_address_id": 1634,
      "addressee_mobile_no": "9819862946",
      "is_lat_long_accurate": false
    },
    "jio_customer_id": null,
    "payment_methods": [
      {
        "slug": "COD",
        "txn_date": "2023-09-05T13:15:40",
        "payment_id": "20",
        "payment_amt": 70.0,
        "payment_cart": null,
        "payment_desc": "COD",
        "bdcustomer_id": null,
        "order_inv_num": null,
        "order_app_code": "",
        "mode_of_payment": "COD",
        "payment_gateway_logo": null,
        "transaction_ref_number": str(order_id)
      }
    ],
    "delivery_charges": [

    ],
    "integration_data": {
      "id": "5ea6821b3425bb07c82a25c1",
      "token": "qO2p_wQkq",
      "config": {
        "app": {
          "id": "5ea6821b3425bb07c82a25c1",
          "__v": 1,
          "_id": "5ea6821b3425bb07c82a25c1",
          "auth": {
            "enabled": true
          },
          "cors": {
            "domains": [

            ]
          },
          "logo": {
            "secure_url": "https://hdn-1.addsale.com/x0/company/164/applications/5efc9913f474c329718e3690/application/pictures/free-logo/original/olqHM8LNr-JioMart-Groceries.png"
          },
          "meta": [

          ],
          "mode": "live",
          "name": "Mobile ASP",
          "owner": "5fa4168649b8806b99ba79d0",
          "token": "qO2p_wQkq",
          "banner": {
            "secure_url": "https://hdn-1.addsale.com/x0/company/164/applications/5efc9913f474c329718e3690/application/pictures/landscape-banner/original/D2fr98CUH-JioMart-Groceries.png"
          },
          "domain": {
            "_id": "5fa417a0b09817296bcd2fc6",
            "name": "jiox3.de",
            "verified": true,
            "is_default": true,
            "is_primary": true,
            "is_shortlink": true
          },
          "secret": "g-smiSo3jY",
          "tokens": [

          ],
          "domains": [
            {
              "_id": "5fa417a0b09817296bcd2fc6",
              "name": "jiox3.de",
              "verified": true,
              "is_default": true,
              "is_primary": true,
              "is_shortlink": true
            },
            {
              "_id": "5fc5f96c008e340ca3480c16",
              "name": "test.jiox3.de",
              "verified": true,
              "is_default": false,
              "is_primary": false,
              "is_shortlink": false
            }
          ],
          "favicon": {
            "secure_url": "https://hdn-1.addsale.com/x0/company/164/applications/5efc9913f474c329718e3690/application/pictures/free-logo/original/olqHM8LNr-JioMart-Groceries.png"
          },
          "website": {
            "enabled": true,
            "basepath": "/"
          },
          "internal": false,
          "cache_ttl": -1,
          "createdAt": "2020-11-05T15:17:52.576Z",
          "is_active": true,
          "updatedAt": "2020-12-01T08:11:16.928Z",
          "company_id": 1,
          "description": "",
          "channel_type": "website-and-mobile-apps",
          "redirections": [

          ]
        },
        "inventory": {
          "__v": 2,
          "_id": "5ea682233425bb02d22a25c2",
          "app": "5ea6821b3425bb07c82a25c1",
          "cart": {
            "enabled": true,
            "bulk_coupons": false,
            "max_cart_items": 50,
            "min_cart_value": 0,
            "delivery_charges": {
              "charges": [

              ],
              "enabled": true
            }
          },
          "order": {
            "enabled": true,
            "force_reassignment": false
          },
          "payment": {
            "source": "JIOMART",
            "enabled": true,
            "methods": {
              "FC": {
                "enabled": false
              },
              "JP": {
                "enabled": false
              },
              "NB": {
                "enabled": true
              },
              "PL": {
                "enabled": true
              },
              "PP": {
                "enabled": false
              },
              "PS": {
                "enabled": true
              },
              "QR": {
                "enabled": true
              },
              "WL": {
                "enabled": true
              },
              "COD": {
                "enabled": false
              },
              "PAC": {
                "enabled": false
              },
              "UPI": {
                "enabled": true
              },
              "CARD": {
                "enabled": true
              },
              "JIOPP": {
                "enabled": false
              },
              "JUSPAYPG": {
                "enabled": false
              },
              "STRIPEPG": {
                "enabled": false
              },
              "PAYUBIZPG": {
                "enabled": false
              },
              "PAYUMONEYPG": {
                "enabled": false
              }
            },
            "cod_charges": 0,
            "callback_url": {
              "app": "",
              "web": ""
            },
            "mode_of_payment": "JIOMART",
            "cod_amount_limit": 49000,
            "payment_selection_lock": {
              "enabled": false,
              "default_options": "",
              "payment_identifier": ""
            }
          },
          "business": "retail",
          "createdAt": "2020-04-27T06:56:35.589Z",
          "inventory": {
            "meta": [
              "standard"
            ],
            "brand": {
              "brands": [
                13,
                16,
                14,
                8,
                12,
                17,
                5,
                1,
                7,
                11,
                15,
                6,
                3,
                18,
                19,
                2,
                10,
                4,
                9
              ],
              "criteria": "explicit"
            },
            "image": [
              "standard"
            ],
            "price": {
              "max": -1,
              "min": 1
            },
            "store": {
              "rules": [
                {
                  "brands": [
                    13,
                    16,
                    14,
                    8,
                    12,
                    17,
                    5,
                    1,
                    7,
                    11,
                    15,
                    6,
                    3,
                    18,
                    19,
                    2,
                    10,
                    4,
                    9
                  ],
                  "companies": [
                    1
                  ]
                }
              ],
              "stores": [

              ],
              "criteria": "filter"
            },
            "category": {
              "criteria": "all",
              "categories": [

              ]
            },
            "discount": {
              "max": -1,
              "min": 0
            },
            "out_of_stock": false
          },
          "logistics": {
            "dp_assignment": true,
            "same_day_delivery": true,
            "logistics_by_seller": false,
            "serviceability_check": true
          },
          "platforms": [

          ],
          "updatedAt": "2020-12-22T11:53:05.791Z",
          "configured": true,
          "comms_enabled": true,
          "reward_points": {
            "debit": {
              "enabled": false,
              "auto_apply": false,
              "strategy_channel": ""
            },
            "credit": {
              "enabled": false
            }
          },
          "authentication": {
            "provider": "fynd",
            "required": true
          },
          "resolvedInventory": {
            "meta": [
              "standard"
            ],
            "brand": {
              "brands": [
                13,
                16,
                14,
                8,
                12,
                17,
                5,
                1,
                7,
                11,
                15,
                6,
                3,
                18,
                19,
                2,
                10,
                4,
                9
              ],
              "criteria": "explicit"
            },
            "image": [
              "standard"
            ],
            "price": {
              "max": -1,
              "min": 1
            },
            "store": {
              "rules": [
                {
                  "brands": [
                    13,
                    16,
                    14,
                    8,
                    12,
                    17,
                    5,
                    1,
                    7,
                    11,
                    15,
                    6,
                    3,
                    18,
                    19,
                    2,
                    10,
                    4,
                    9
                  ],
                  "companies": [
                    1
                  ]
                }
              ],
              "stores": [

              ],
              "criteria": "filter"
            },
            "category": {
              "criteria": "all",
              "categories": [

              ]
            },
            "discount": {
              "max": -1,
              "min": 0
            },
            "out_of_stock": false
          },
          "article_assignment": {
            "rules": {
              "store_priority": {
                "enabled": false,
                "storetype_order": [

                ]
              }
            },
            "enforced_stores": [

            ],
            "post_order_reassignment": true
          }
        }
      }
    },
    "jiomart_order_id": str(order_id),
    "shipping_address": {
      "lat": 18.900297285,
      "lon": 72.816089557,
      "pin": "400005",
      "area": "",
      "city": "Mumbai",
      "state": "Maharashtra",
      "sector": "Test Account",
      "street": "Test Account",
      "flat_no": "NA",
      "plot_no": "",
      "block_no": "",
      "floor_no": "NA",
      "tower_no": null,
      "addresstype": "home",
      "apartment_id": null,
      "society_name": "",
      "building_name": "NA",
      "building_type": "apartment",
      "addressee_name": "Pratik  Bambulkar",
      "addressee_mobile_no": "9819862946",
      "shipping_address_id": 1634,
      "is_lat_long_accurate": false
    },
    "stormbreaker_uuid": "2832afe4-18a9-4509-a749-eace2e679318",
    "order_products_sku": [
      "RVUWL2CSFA"
    ]
  })
    headers = {
        'x-integration-id': '5ea6821b3425bb07c82a25c1',
        'x-integration-token': 'qO2p_wQkq',
        'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    print(response.text)

    with open("create_order.csv", 'a') as file:
        content_files = csv.writer(file)
        content_files.writerow(
            [str(order_id), payload, str(response.status_code),
             str(response.text),
             str(datetime.now())])
        file.close()


count = 0
for order_no in range(11095208460000010, 17895208460000007):
    create_order(str(order_no)+"W")
    count = count + 1
    if count == 2000:
        break




