import requests
import json
import time


def assign_dp(shipment_id):
    url = "https://api.jioecomm.com/__jiomart/oms/fulfillment/api/v1/oms/manual-place-shipment/"

    payload = json.dumps({
        "shipment_ids": [
            shipment_id
        ],
        "dp_id": "20"
    })
    headers = {
        'cache-control': 'no-cache',
        'Content-Type': 'application/json',
        'Cookie': 'f.session=s%3AD721s4EKHSVe5jXKkoHiGaBlhVixNGaI.UbXLu3s0ysJoZsNOAvfFjWRVhNVpj4%2FZRDNgJVbUNWc'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    print(response.text)


shipment_idss = ['16955259453361604641J']

for ids in shipment_idss:
    assign_dp(ids)
    # time.sleep(0.5)
