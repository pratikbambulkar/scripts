import pip
import os


def install(package):
    if hasattr(pip, 'main'):
        pip.main(['install', package])
    else:
        pip._internal.main(['install', package])


if __name__ == '__main__':
    install('pandas')
    install('csv')

import json
import csv
import requests
from datetime import datetime
import pandas as pd



def dummy(awb_no, shipment_id, shipping_agent, return_id, shipping_charge):

    url = "https://ril-prod-sellow-vpce.ril.smebazaar.ooo/pvt/api/returns/update_status.json"

    payload = json.dumps({

        "waybill": [
            str(awb_no)
        ],
        "return_id": str(return_id),
        "tracking_code": str(awb_no),
        "shipment_id": str(shipment_id),
        "status": "pickup_initiated",
        "shipping_agent_code": str(shipping_agent),
        "shipping_charge": str(shipping_charge)

    })

    headers = {
        'Content-Type': 'application/json',
        'Testing': 'e1e50803314fade1dd8a930cd45bf3ce',
        'Authorization': 'Bearer 95d12a059cf8d7644ece37d30daad6bc48c55fa3003cad57f77a80b4b8e3449e'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    print(response.text)

    print(response.status_code)

    with open("dummy_dp_assigned_logs.csv", 'a') as file:
        content_files = csv.writer(file)
        content_files.writerow(
            [str(shipment_id), str(return_id), payload, str(response.json()), str(response.status_code),
             str(response.text),
             str(datetime.now())])
        file.close()


with open("dummy_dp_assigned_logs.csv", 'a') as cf:
    content_file = csv.writer(cf)
    content_file.writerow(
        ["shipment_id", "Return_id", "Payload", "Response_text", "Status code", "Response_text", "date_time"])
    cf.close()

data = pd.read_csv('dummy_RDP.csv')

for i, j, k, l, m in zip(data['shipment_id'],
                      data['awb_no'],
                      data['return_id'],
                      data['shipping_agent'],
                      data['shipping_charge']
                      ):
    dummy(awb_no=j, shipment_id=i, shipping_agent=l, return_id=k, shipping_charge=m)

print('done')
