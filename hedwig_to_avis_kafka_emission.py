from hedwig_v2.eventbridge.emitter import EventEmitter
data = [{"shipment_id": "16439040379461845726J", "awb": 2904987978}]
dp_id = 1
dp_name = "ecom_jio"
updated = {}
#statuses = ("bag_not_picked","")
#statuses = ("bag_not_picked",out_for_pickup","")
#statuses = ("bag_not_picked",out_for_pickup","bag_picked","")
#statuses = ("bag_not_picked",out_for_pickup","bag_picked","in_transit","")
#statuses = ("bag_not_picked",out_for_pickup","bag_picked","in_transit","out_for_delivery","")
#statuses = ("bag_not_picked",out_for_pickup","bag_picked","in_transit","out_for_delivery","delivery_done","")
#statuses = ("bag_not_picked",out_for_pickup","bag_picked","in_transit","delivery_attempt_failed","rto_initiated","")
#statuses = ("bag_not_picked",out_for_pickup","bag_picked","in_transit","delivery_attempt_failed","rto_initiated","rto_bag_delivered","")
#statuses = ("out_for_pickup","")
#statuses = ("out_for_pickup","bag_picked","")
#statuses = ("out_for_pickup","bag_picked","in_transit","")
#statuses = ("out_for_pickup","bag_picked","in_transit","out_for_delivery","")
statuses = ("out_for_pickup", "bag_picked", "in_transit", "out_for_delivery", "delivery_done", "")
#statuses = ("out_for_pickup","bag_picked","in_transit","delivery_attempt_failed","rto_initiated","")
#statuses = ("out_for_pickup","bag_picked","in_transit","delivery_attempt_failed","rto_initiated","rto_bag_delivered","")
#statuses = ("bag_picked","")
#statuses = ("bag_picked","in_transit","")
#statuses = ("bag_picked","in_transit","out_for_delivery","")
#statuses = ("bag_picked","in_transit","out_for_delivery","delivery_done","")
#statuses = ("bag_picked","in_transit","delivery_attempt_failed","rto_initiated","")
#statuses = ("bag_picked","in_transit","delivery_attempt_failed","rto_initiated","rto_bag_delivered","")
#statuses = ("in_transit","")
#statuses = ("in_transit","out_for_delivery","")
#statuses = ("in_transit","out_for_delivery","delivery_done","")
#statuses = ("in_transit","out_for_delivery","delivery_attempt_failed","rto_initiated","")
#statuses = ("in_transit","out_for_delivery","delivery_attempt_failed","rto_initiated","rto_bag_delivered","")
#statuses = ("out_for_delivery","")
#statuses = ("out_for_delivery","delivery_done","")
#statuses = ("out_for_delivery","delivery_attempt_failed","rto_initiated","")
#statuses = ("out_for_delivery","delivery_attempt_failed","rto_initiated","rto_bag_delivered","")
#statuses = ("delivery_done","")
#statuses = ("delivery_attempt_failed","rto_initiated","")
#statuses = ("delivery_attempt_failed","rto_initiated","rto_bag_delivered","")
#statuses = ("dp_assigned","bag_not_picked","out_for_pickup","bag_picked","in_transit","out_for_delivery","delivery_done","")
#statuses = ("dp_assigned","bag_not_picked","out_for_pickup","bag_picked","in_transit","delivery_attempt_failed","rto_initiated","rto_bag_delivered","")
#statuses = ("return_dp_assigned","")
#statuses = ("return_dp_assigned","return_bag_not_picked","")
#statuses = ("return_dp_assigned","return_bag_not_picked","return_dp_out_for_pickup","")
#statuses = ("return_bag_not_picked","")
#statuses = ("return_bag_not_picked","return_dp_out_for_pickup","")
#statuses = ("return_dp_out_for_pickup","")
#statuses = ("rto_bag_delivered","")

for status in statuses:
    for item in data:
        shipment_id = item["shipment_id"]
        awb = item["awb"]
        debug_info = {}
        bag_status = {
            'bag_id': shipment_id,
            'shipment_id': shipment_id,
            'bag_status': status,
            'shipment_status': status,
            'success': True,
            'awb_number': awb,
            'dp_id': dp_id,
            'dp_name': dp_name,
            'dp_details': {
                'id': dp_id,
                'name': dp_name
            },
            'meta': {
                'ewaybill_info': {
                }
            }
        }
        shipment_status_event_emitter = EventEmitter('shipment_status')
        response = shipment_status_event_emitter.emit(bag_status, debug_info=debug_info)
        print(shipment_id, status)