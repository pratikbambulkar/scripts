import psycopg2.extras
import csv
import os

os.remove('failed.csv')
os.remove('hogwarts_data.csv')
os.remove('output.csv')

# import mysql.connector # to connect with mysql server.
# establishing the connection
conn = psycopg2.connect(
    database="hogwarts", user='adhoc_user', password='CW4k75c24vc23D', host='jioecomm-hogwarts-rr-1.cgszbyyjeamy.ap'
                                                                            '-south-1.rds.amazonaws.com', port='5432')

# Setting auto commit false
conn.autocommit = False

# Creating a cursor object using the cursor() method
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

# taking the failed data
query = f"""select 
  primary_identifier, 
  uid, 
  state, 
  status, 
  created_on, 
  state_index, 
  to_jsonb(data :: jsonb) -> '_original_data' -> 'fulfilling_store' -> 'meta' -> '_custom_json' -> 'jio_sale_point' as JSP
from 
  task 
where 
  integration_id = 5 
  and status = 'failed' 
  and created_on between '2023-06-20 00:00:00' 
  and '2023-06-26 23:59:59' 
  and data -> '_original_data' -> 'fulfilling_store' -> 'meta' -> '_custom_json' ->> 'jio_sale_point' not in ('3P');"""


cursor.execute(query)
avis = cursor.fetchall()
avis_fp = open("failed.csv", "w+")
avis_my_file = csv.writer(avis_fp)
avis_my_file.writerow(["primary_identifier", "uid", "state", "status", "created_on", "state_index", "JSP"])
avis_my_file.writerows(avis)
avis_fp.close()

# taking the shipments from the failed.csv
shipments_list = []
file = open("failed.csv")
csvreader = csv.reader(file)
for index, row in enumerate(csvreader):
    if index == 0:
        continue
    shipments_list.append(row[0])


# Fetching the failed response from task_events by joining task and task_events
fp = open("hogwarts_data.csv", "a")
myFile = csv.writer(fp)
myFile.writerow(["primary_identifier", "payload", "response_text", "external_call_on"])


for index, shipment in enumerate(shipments_list):
    query = f"""select 
  tk.primary_identifier, 
  te.payload, 
  te.response_text, 
  te.external_call_on 
from 
  task tk 
  join task_events te ON te.task_id = tk.uid 
where 
  integration_id = 5 
  and tk.primary_identifier = '{shipment}' 
  and tk.status = 'failed' 
order by 
  external_call_on desc 
limit 
  1
; """
    cursor.execute(query)
    hog = cursor.fetchall()

    # If we don't want our data in csv file we could just simply print result
    # print(avs)

    # this way we can print data in csv format.
    if index == 0:
        myFile.writerows(hog)

    else:
        fp = open("hogwarts_data.csv", "a")
        myFile = csv.writer(fp)
        myFile.writerows(hog)
        fp.close()


# Doing V-lookup on two sheets
def vlookup(csv_file1, csv_file2, key_column, lookup_columns, output_file):
    # Read the first CSV file and create a dictionary with the key column as the key
    data_dict = {}
    with open(csv_file1, 'r') as file1:
        reader1 = csv.DictReader(file1)
        for row in reader1:
            key = row[key_column]
            data_dict[key] = row

    # Read the second CSV file and append the lookup columns from the first file
    result = []
    with open(csv_file2, 'r') as file2:
        reader2 = csv.DictReader(file2)
        for row in reader2:
            key = row[key_column]
            lookup_values = data_dict.get(key)
            for col in lookup_columns:
                row[col] = lookup_values.get(col)

            result.append(row)

    # Write the updated rows to the output CSV file
    with open(output_file, 'w', newline='') as outfile:
        writer = csv.DictWriter(outfile, fieldnames=reader2.fieldnames + lookup_columns)
        writer.writeheader()
        writer.writerows(result)


# Example usage
csv_file1 = 'hogwarts_data.csv'  # Replace with the path to your first CSV file
csv_file2 = 'failed.csv'  # Replace with the path to your second CSV file
key_column = 'primary_identifier'  # Replace with the column name used as the key for matching
lookup_columns = ['payload', 'response_text', 'external_call_on']
# Replace with the column name to be looked up and appended

output_file = 'output.csv'  # Replace with the desired output file path

vlookup(csv_file1, csv_file2, key_column, lookup_columns, output_file)
