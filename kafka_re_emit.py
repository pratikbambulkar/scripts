shipments = ['16370529481991530907J']
for ids in shipments:
    shipment_ids = [ids]
    from models import *

    session = Backend().get_session()
    sss = session.query(ShipmentStatus).filter(ShipmentStatus.shipment_id.in_(shipment_ids)).all()
    shipment_updated_time = time()
    for ss in sss:
        if not ss.meta or not ss.meta.get("kafka_emission_status"):
            KafkaProducerHelper.emit_tasks(
                methods=["emit_shipment_status_event"],
                partition_key=ss.shipment_id,
                shipment_id=ss.shipment_id,
                shipment_status_id=ss.id,
                shipment_updated_time=shipment_updated_time,
                action="shipment_status_update",
                status=ss.status
            )












shipments = ['16263557165671117567J','16264505017621070523J','16276485494921828715J','16302079659191618369J','16311938903671686483J','16315997220981954041J','16325745941001180545J','16344931328351890131J','16381858478571915669J','16385611217181203980J','16423299669951918352J','16434545629601452884J','16464074980691238572J','16464076420831201095J','16492512342721079166J','16494173313531345969J','16494365327781025780J','16502789467741973167J','16502791701011996169J','16502854436921280000J','16502974096911991916J','16508995341141059771J','16519383672951030737J','16526939406651692025J','16540765130171721350J','16549409926121241481J','16552158334791747502J','16560699825711042364J','16564061774941564440J','16564129171721572589J','16704206307341002461J','16781756362041721385J']
for ids in shipments:
    shipment_ids = [ids]
    from models import *

    session = Backend().get_session()
    sss = session.query(ShipmentStatus).filter(ShipmentStatus.shipment_id.in_(shipment_ids)).all()
    shipment_updated_time = time()
    for ss in sss:
        print(ss.status)
        if ss.status == 'return_initiated':
            KafkaProducerHelper.emit_tasks(
                methods=["emit_shipment_status_event"],
                partition_key=ss.shipment_id,
                shipment_id=ss.shipment_id,
                shipment_status_id=ss.id,
                shipment_updated_time=shipment_updated_time,
                action="shipment_status_update",
                status=ss.status
            )

            print("emitted")
