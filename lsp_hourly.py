#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 11 12:37:14 2022

@author: chirayukainya
"""

import functions_framework

from datetime import datetime, timedelta
import sys
import pandas as pd
import json

import time
import requests
import pygsheets
from google.cloud import bigquery


def func_19_remarks(x):
    try:
        return x['packages'][0]['remarks'][0]
    except:
        return "NA"


def func_19_reason(x):
    try:
        return x['rmk']
    except:
        return "NA"


def func_19_pincode(x):
    try:
        return x.split("'")[1][:6]
    except:
        return "NA"


def func_1_remarks(x):
    return "NA"


def func_1_reason(x):
    try:
        return x['shipments'][0]['reason']
    except:
        return "NA"


def func_1_pincode(x):
    try:
        return x[39:45]
    except:
        return "NA"


gc = pygsheets.authorize(service_account_file='client_secret.json')
sh = gc.open_by_key('1T_JEueokcVthVAznGcSIeEpH8g1GqrVw4lMjNxBxaW8')
wks = sh.worksheet_by_title('Dump_all')
data = pd.DataFrame(wks.get_all_values())
data.columns = ['shipment_id', 'created_at(hedwig)', 'raw_response', 'shipment_type', 'dp_name', 'Reason', 'Remarks',
                'not_servicable_pincode']
data.drop([0], axis=0, inplace=True)
data = data[data['shipment_id'] != '']

gc = pygsheets.authorize(service_account_file='client_secret.json')
sh = gc.open_by_key('1T_JEueokcVthVAznGcSIeEpH8g1GqrVw4lMjNxBxaW8')
wks = sh.worksheet_by_title('Dump')
data11 = pd.DataFrame(wks.get_all_values())
data11.columns = ['shipment_id', 'response']
data11.drop([0], axis=0, inplace=True)
data11 = data11[data11['shipment_id'] != '']

from_date = (datetime.today() - timedelta(days=7)).date()
to_date = datetime.today().date()

if len(sys.argv) <= 1:
    to_date = str(to_date) + " 23:59:59"
    from_date = str(from_date) + " 00:00:00"

    from_date = datetime.strptime(from_date, '%Y-%m-%d %H:%M:%S')
    to_date = datetime.strptime(to_date, '%Y-%m-%d %H:%M:%S')
else:

    from_date = datetime.strptime(sys.argv[1], '%Y-%m-%d %H:%M:%S')
    to_date = datetime.strptime(sys.argv[2], '%Y-%m-%d %H:%M:%S')


def check_func():
    # db_connection_avis = sql.connect(host='jioecomm-avis-rr-1.cgszbyyjeamy.ap-south-1.rds.amazonaws.com', user='fynd_avis', database ='avis',password='avis_readWrite_retail!2017')

    query = """select s.id as shipment_id ,ss.status from jiomart.jioecomm_main_dataset_v1.avis_shipment s 
    inner join jiomart.jioecomm_main_dataset_v1.avis_shipment_status ss on s.current_shipment_status=ss.id 
    where status in ('return_initiated','rto_initiated','bag_packed')
    and bag_list != '' and ss.created_at >= '""" + str(from_date) + """' 
    and s.created_at>= '""" + str(from_date) + """' """

    shipment_details_avis = pd.read_gbq(query, project_id='jiomart', dialect='standard')

    # shipment_details_avis = pd.read_sql(query,con=db_connection_avis)

    # db_connection_avis.close()

    shipments = list(data['shipment_id'].unique()) + list(shipment_details_avis['shipment_id'].unique())

    start_index = 0
    bucket = 1000
    shipment_details_hedwig_temp = pd.DataFrame(columns=['shipment_id', 'id'], index=range(1))

    for i in range((len(shipments) // bucket) + 1):
        if i == 0:
            end_index = bucket
        else:
            end_index = start_index + bucket * i

        if len(shipments[start_index:end_index]) > 0:
            # db_connection_hedwig = sql.connect(host='jioecomm-hedwig-rr-1.cgszbyyjeamy.ap-south-1.rds.amazonaws.com', user='hedwig', database ='hedwig',password='fyndDbMySQL#kjhkjh')

            query = """select shipment_id,max(id) as id from jiomart.jioecomm_main_dataset_v1.hedwig_request_logger 
            where
            shipment_id in (""" + ''.join("'" + e + "'" + "," for e in shipments[start_index:end_index])[:-1] + """) 
            and created_at>= '""" + str(from_date) + """'
            group by 1"""

            shipment_details_hedwig_temp = shipment_details_hedwig_temp.append(
                pd.read_gbq(query, project_id='jiomart', dialect='standard'), ignore_index=True)

        start_index = end_index
    # shipment_details_hedwig_temp = pd.read_sql(query,con=db_connection_hedwig)

    # db_connection_hedwig = sql.connect(host='jioecomm-hedwig-rr-1.cgszbyyjeamy.ap-south-1.rds.amazonaws.com', user='hedwig', database ='hedwig',password='fyndDbMySQL#kjhkjh')

    start_index = 0
    bucket = 1000
    shipment_details_hedwig = pd.DataFrame(
        columns=['shipment_id', 'account_id', 'created_at', 'raw_response', 'shipment_type'], index=range(1))

    ids_unique = shipment_details_hedwig_temp[
        ~((shipment_details_hedwig_temp['id'].isna()) | (shipment_details_hedwig_temp['id'].isnull()))]['id'].unique()

    for i in range((len(shipments) // bucket) + 1):
        if i == 0:
            end_index = bucket
        else:
            end_index = start_index + bucket * i

        if len(ids_unique[start_index:end_index]) > 0:
            query = """select shipment_id,account_id,created_at,raw_response,shipment_type
            from jiomart.jioecomm_main_dataset_v1.hedwig_request_logger 
            where (raw_response like '%non serviceable pincode%' 
                OR raw_response like '%Client-Warehouse%'
                OR  raw_response like '%not serviceable%'
                OR raw_response like '%"success": false%'
                OR raw_response like '%not operational%') 
            and  
            id in (""" + ''.join(str(e) + "," for e in ids_unique[start_index:end_index])[:-1] + """) and 
            created_at >= '""" + str(from_date) + """'"""

            shipment_details_hedwig = shipment_details_hedwig.append(
                pd.read_gbq(query, project_id='jiomart', dialect='standard'), ignore_index=True)

        start_index = end_index
    query_1 = """select id as account_id,name as dp_name from jiomart.jioecomm_main_dataset_v1.hedwig_account"""

    shipment_details_hedwig_1 = pd.read_gbq(query_1, project_id='jiomart', dialect='standard')

    shipment_details_hedwig_1 = shipment_details_hedwig_1[~shipment_details_hedwig_1['account_id'].isna()]

    # shipment_details_hedwig_1 = shipment_details_hedwig_1[shipment_details_hedwig_1['account_id'].notna()]

    # shipment_details_hedwig = pd.read_sql(query,con=db_connection_hedwig)

    # shipment_details_hedwig_1 = pd.read_sql(query_1,con=db_connection_hedwig)

    shipment_details_hedwig = shipment_details_hedwig.merge(shipment_details_hedwig_1, on=['account_id'], how='left')

    # db_connection_hedwig.close()

    shipment_details_hedwig['raw_response'] = shipment_details_hedwig['raw_response'].apply(
        lambda x: 'Na' if str(x) == 'nan' else json.loads(x))

    shipment_details_hedwig['account_id'] = shipment_details_hedwig['account_id'].apply(
        lambda x: 'NA' if pd.isnull(x) else int(x))

    shipment_details_hedwig_19 = shipment_details_hedwig[shipment_details_hedwig['account_id'] == 19]

    shipment_details_hedwig_1 = shipment_details_hedwig[shipment_details_hedwig['account_id'] == 1]

    shipment_details_hedwig_others = shipment_details_hedwig[~shipment_details_hedwig['account_id'].isin([19, 1])]

    shipment_details_hedwig_19['Remarks'] = shipment_details_hedwig_19['raw_response'].apply(
        lambda x: func_19_remarks(x))

    shipment_details_hedwig_19['Reason'] = shipment_details_hedwig_19['raw_response'].apply(lambda x: func_19_reason(x))

    shipment_details_hedwig_19['not_servicable_pincode'] = shipment_details_hedwig_19['Remarks'].apply(
        lambda x: func_19_pincode(x))

    shipment_details_hedwig_1['Reason'] = shipment_details_hedwig_1['raw_response'].apply(lambda x: func_1_reason(x))

    shipment_details_hedwig_1['Remarks'] = "NA"

    shipment_details_hedwig_1['not_servicable_pincode'] = shipment_details_hedwig_1['Reason'].apply(
        lambda x: func_1_pincode(x))

    shipment_details_hedwig_others['Remarks'] = "NA"
    shipment_details_hedwig_others['Reason'] = "NA"
    shipment_details_hedwig_others['not_servicable_pincode'] = "NA"

    shipment_details_hedwig_f = shipment_details_hedwig_1.append(shipment_details_hedwig_19)
    shipment_details_hedwig_f = shipment_details_hedwig_f.append(shipment_details_hedwig_others)

    if len(shipment_details_hedwig) == len(shipment_details_hedwig_f):
        print(True)
    else:
        print(False)

    shipment_details_hedwig_f.drop(columns=['account_id'], axis=1, inplace=True)

    return shipment_details_hedwig_f


push_df = pd.DataFrame(columns=['Shipment_id', 'response_text'], index=range(1))


def repush(df, df1):
    df['created_at'] = df['created_at'].apply(
        lambda x: 'NA' if pd.isnull(x) else datetime.strptime(str(x).split("+")[0], '%Y-%m-%d %H:%M:%S'))
    df = df[df['created_at'] != 'NA']
    df = df[df['created_at'] <= (datetime.today() - timedelta(days=3))]

    url = "https://api.jioecomm.com/__jiomart/oms/fulfillment/api/v1/oms/manual-place-shipment/"

    shipment_ids = df[~df['shipment_id'].isin(df1['shipment_id'])]['shipment_id'].unique()

    if len(shipment_ids) > 0:
        query = """select s.id as shipment_id from jiomart.jioecomm_main_dataset_v1.avis_shipment s 
        inner join jiomart.jioecomm_main_dataset_v1.avis_shipment_status ss on s.current_shipment_status=ss.id 
        where status in ('return_initiated','rto_initiated','bag_packed')
        and bag_list != ''
        and s.id in (""" + ''.join("'" + e + "'" + "," for e in shipment_ids)[:-1] + """)
        and ss.created_at >= '""" + str(from_date) + """'
        and s.created_at>='""" + str(from_date) + """'"""

        shipment_details_avis = pd.read_gbq(query, project_id='jiomart', dialect='standard')

        shipment_ids = shipment_details_avis['shipment_id'].unique()

    if len(shipment_ids) > 0:

        for id in shipment_ids:
            payload = json.dumps({
                "shipment_ids": [
                    id
                ],

            })
            headers = {
                'cache-control': 'no-cache',
                'Content-Type': 'application/json',
                'Cookie': 'f.session=s%3Az_6Vwzp7mB2AmDPTbBdty54--1IkNUOx.RwkClEScncQUaPbAvD%2BNpsZZ2AtNg64aF8LXU19lDCM'
            }

            response = requests.request("POST", url, headers=headers, data=payload)
            print(response.text)

            global push_df

            push_df_temp = pd.DataFrame(columns=['Shipment_id', 'response_text'], index=range(1))

            push_df_temp['Shipment_id'] = id
            push_df_temp['response_text'] = json.loads(response.text)['response'][0]['message']

            push_df = push_df_temp.append(push_df_temp)

            push_df.drop_duplicates(inplace=True)
            push_df.reset_index(inplace=True, drop=True)

        gc = pygsheets.authorize(service_account_file='client_secret.json')
        sh = gc.open_by_key('1T_JEueokcVthVAznGcSIeEpH8g1GqrVw4lMjNxBxaW8')
        wks = sh.worksheet_by_title('Dump')
        data1 = pd.DataFrame(wks.get_all_values())
        data1.columns = ['Shipment_id', 'Response']
        data1.drop([0], axis=0, inplace=True)
        data1 = data1[data1['Shipment_id'] != '']

        gc = pygsheets.authorize(service_account_file='client_secret.json')
        sh = gc.open_by_key('1T_JEueokcVthVAznGcSIeEpH8g1GqrVw4lMjNxBxaW8')
        wks = sh.worksheet_by_title('Dump')
        # print(req)
        wks.set_dataframe(start='A' + str(len(data1) + 2), df=push_df, copy_head=False)

        time.sleep(10 * 60)


def update_gs(log):
    gc = pygsheets.authorize(service_account_file='client_secret.json')
    sh = gc.open_by_key('1T_JEueokcVthVAznGcSIeEpH8g1GqrVw4lMjNxBxaW8')
    wks = sh.worksheet_by_title('Dump_all')
    wks.clear(start='A2')
    log = log[~log['shipment_id'].isna()]

    if len(log) > 0:
        query = """select s.id as shipment_id from jiomart.jioecomm_main_dataset_v1.avis_shipment s 
        inner join jiomart.jioecomm_main_dataset_v1.avis_shipment_status ss on s.current_shipment_status=ss.id 
        where status in ('return_initiated','rto_initiated','bag_packed')
        and bag_list != ''
        and s.id in (""" + ''.join("'" + str(e) + "'" + "," for e in log['shipment_id'].unique())[:-1] + """)
        and s.created_at >= '""" + str(from_date) + """'        
        and ss.created_at >= '""" + str(from_date) + """'"""

        shipment_details_avis = pd.read_gbq(query, project_id='jiomart', dialect='standard')

        log = log[log['shipment_id'].isin(shipment_details_avis['shipment_id'])]

        wks.set_dataframe(start='A2', df=log, copy_head=False)


@functions_framework.http
def entrypoint(request):
    df = check_func()
    repush(df, data11)
    df1 = check_func()

    if len(df1) > 0:
        update_gs(df1)
    return 'Done'
