import requests
import json

url = "https://api.jioecomm.com/hogwarts/api/v1/manual-retry-api"

uids = ['551086eb-605e-462c-9785-f757ee789c81','b9a5dac6-68cf-477e-b31a-1532c5741b40','0636da99-ad34-4dcf-b0d9-6240d98fae37','c640e7c7-03cc-412b-9888-aa1e44c94613','840fe9f0-356b-4338-8f3f-2b879b2ca988','1c8eb3bb-2db6-4e8d-a9de-34396c8a7526','1134e596-c5a0-495f-be6d-e2daea8fe33e','2343d157-cbaf-41b3-a64e-dee805382778','a6858f56-d2f4-4def-a04a-e02817d0307a','720d753f-4061-4d61-9646-32f44ae020c7','942cb1fa-de8e-49df-a822-c532ebfbb02c','48106379-01a0-495f-b6d0-6e4b9e6b46b8','1194cfe4-45ad-4c7d-b856-5173a4591b5e','efdf793a-df71-4014-bd7b-06aa2e0d89ba','7a8ba2f6-e650-4a21-9b16-3112bc4aceef','71656908-66df-4cc5-a26c-e1e33ef7b050','ce81096b-82aa-45bb-a68f-c3ddf798a870','9a6b8687-0954-4ebe-8df9-13e805ec37b5','1ed18add-c700-41a1-9beb-4696655217d3','00963118-9c0c-45c2-9ac5-6fb52bd4817e','8edff23a-af3a-4954-aea6-dd56e7b8b58d','12acd4db-4c3e-45bb-a594-c02f9b9e3e7c','80c23031-5364-4e25-b9a4-956fb2e75b6c']

for uid in uids:
    payload = json.dumps({
      "uid": [
        uid
      ]
    })
    headers = {
      'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    print(response.text)
