import requests
import json

for i in range(100):
    url = "https://api.jioecomm.com/__jiomart/oms/view/order/api/v1/shipments-external"
    payload = json.dumps({
      "group_entity": "shipments",
      "sub_group_entity": "bags",
      "page_no": 1,
      "page_size": 50,
      "filter_required": True,
      "type_of_req": "order_header",
      "timefilter": None,
      "order_status": None,
      "show_leads": True,
      "fields": [
        "source",
        "shipment_id",
        "shipment_status",
        "order",
        "expected_delivery_date",
        "itemDetails",
        "order_created_time",
        "channel_type",
        "vertical",
        "pay_button",
        "ship_type"
      ]
    })
    headers = {
      'X-Checksum': 'b2ae39d1af8e491d8da5c1123970762785a9e8cc4521bfeb23adcaf8cbb14476',
      'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJoYXNoIjoiNmI0ODg2ZDE2M2ZjZjAzNzkwM2MwYzY2NGM3OTU5ODIzYzk5MGExYjNhMDU5ZjkzZjRjZTM4OWI4ZTBmYzZlYyIsImlhdCI6MTY3OTY1MjExNiwiZXhwIjoxNjc5NzM4NTE2LCJzYWx0IjowfQ.uAlsNBjUuSsOkMnSrzaXF4E6QhraN2MyfMzwb7i7r4w',
      'customer_id': '115899428',
      'Content-Type': 'application/json'
    }
    response = requests.request("POST", url, headers=headers, data=payload)
    print(response.text)
