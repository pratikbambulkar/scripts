import csv
import mysql.connector as sql  # to connect with mysql server pip install mysql-connector
import os

os.remove("pincode_issue.csv")

conn = sql.connect(
    user="hedwig",
    password="fyndDbMySQL#kjhkjh",
    host="jioecomm-hedwig-rr-1.cgszbyyjeamy.ap-south-1.rds.amazonaws.com",
    database="hedwig",
)

fp = open("pincode_issue.csv", "a")
myFile = csv.writer(fp)
myFile.writerow(["shipment_id", "dp", "XPB_response", "delhivery_response", "shadowfax_response", "delhivery_remark", "ecom_response"])

# Setting auto commit false
conn.autocommit = False

# Creating a cursor object using the cursor() method
cursor = conn.cursor()

shipments_list = ['16954824529591351186J','16955259453361604641J','16955289923911833542J']

for index, shipment in enumerate(shipments_list):
    query = f"""select rl.shipment_id,acc.name as Delivery_Partner, 
    json_extract(rl.raw_response,'$.ReturnMessage') as XPB_response,
    json_extract(rl.raw_response,'$.packages[0].remarks') as delhivery_response,
    json_extract(rl.raw_response,'$.errors') as shadowfax_response,
    json_extract(rl.raw_response,'$.rmk') as delhivery_remark,
    json_extract(rl.raw_response,'$.shipments[0].reason') as ecom_response
    from request_logger as rl INNER JOIN account as acc ON rl.account_id = acc.id where rl.shipment_id = '{shipment}' 
    order by rl.id desc limit 1; """
    cursor.execute(query)
    avs = cursor.fetchall()

    # If we don't want our data in csv file we could just simply print result
    # print(avs)

    # this way we can print data in csv format.
    if index == 0:
        myFile.writerows(avs)

    else:
        fp = open("pincode_issue.csv", "a")
        myFile = csv.writer(fp)
        myFile.writerows(avs)
        fp.close()
