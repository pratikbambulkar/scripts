import csv
import ast
import json

file = open("QR_1.csv")
csvreader = csv.reader(file)

fp = open("QR_final.csv", "a")
myFile = csv.writer(fp)

for index, row in enumerate(csvreader):
    if index == 0:
        continue
    shipment_id, cod, data = row
    payload = ast.literal_eval(data)
    entry = [
        shipment_id,
        cod,
        payload["shipping_charge"],
        payload["shipping_agent_code"],
        payload["payment_qr_code_url"],
        payload["waybill"],
        payload["to_pincode"],
        payload["shipping_status"],
        payload["tracking_code"],
    ]

    # Convert the list to JSON string
    json_entry = json.dumps(entry)
    # Write the JSON string to the file
    fp.write(json_entry + "\n")
