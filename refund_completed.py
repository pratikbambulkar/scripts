import pip
import ast
import os

os.remove("refund_completed_logs.csv")

def install(package):
    if hasattr(pip, 'main'):
        pip.main(['install', package])
    else:
        pip._internal.main(['install', package])


if __name__ == '__main__':
    install('pandas')
    install('csv')

import json
import csv
import requests
from datetime import datetime
import pandas as pd


def refund_completed_api_push(shipment_id, refund_source_id, refund_amount, payment_methods):
    url = "https://ril-prod-sellow-vpce.ril.smebazaar.ooo/pvt/api/shipments/update_refund_status.json"

    payload = json.dumps({
            "status": "complete",
            "refund_source_id": refund_source_id,
            "refund_amount": int(refund_amount),
            "shipment_id": shipment_id,
            "payment_methods": ast.literal_eval(payment_methods)

    })

    headers = {
        'Content-Type': 'application/json',
        'Testing': 'e1e50803314fade1dd8a930cd45bf3ce',
        'Authorization': 'Bearer 188e56a89f686cb9c7612316d69618e878d8f9af41e9b1abfa4873059e5e8bf9'
    }

    print(payload)
    response = requests.request("POST", url, headers=headers, data=payload)

    print(response.text)

    print(response.status_code)

    with open("refund_completed_logs.csv", 'a') as file:
        content_files = csv.writer(file)
        content_files.writerow(
            [str(shipment_id), str(refund_source_id), payload, str(response.status_code),
             str(response.text),
             str(datetime.now())])
        file.close()


with open("refund_completed_logs.csv", 'a') as cf:
    content_file = csv.writer(cf)
    content_file.writerow(
        ["shipment_id", "Return_id", "Payload", "Status code", "Response_text", "date_time"])
    cf.close()

refund_completed = pd.read_csv('refund_completed.csv')

for i, j, k, l in zip(refund_completed['refund_source_id'],
                      refund_completed['refund_amount'],
                      refund_completed['shipment_id'],
                      refund_completed['payment_methods']):

    refund_completed_api_push(refund_source_id=i, refund_amount=j, shipment_id=k, payment_methods=l)

print('done')


