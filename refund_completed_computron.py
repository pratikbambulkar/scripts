import logging
import json, requests
import logging
import time
import csv
from datetime import datetime

from config import current_config
from pymongo import MongoClient


with open("refund_completed_logs_saved.csv", 'a') as cf:
    content_file = csv.writer(cf)
    content_file.writerow(
        ["shipment_id", "Return_id", "Payload", "Response_text", "Status code", "date_time"])
    cf.close()


# # Configure logging
# logging.basicConfig(,
#                     format='%(asctime)s %(levelname)s %(message)s')

# Set up logger
# logging.basicConfig(
#     filename='refund_request_to_IB.txt',
#     level=logging.DEBUG, filemode='w',
#     format='%(name)s - %(levelname)s - %(message)s'
# )
logger = logging.getLogger(__name__)

logger.setLevel(logging.DEBUG)

# Create a file handler
file_handler = logging.FileHandler('refund_postings_to_IB_batch.txt', 'a')
file_handler.setLevel(logging.DEBUG)

# Create a formatter and add it to the file handler
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
file_handler.setFormatter(formatter)

# Add the file handler to the logger
logger.addHandler(file_handler)

connection_uri = current_config.MONGO_URI
client = MongoClient(connection_uri)
db = client.computron

"""
{
  "refund_source_id": "16831091426501223560J" -> new_shipment_id,
  "shipment_id": "16823908151871792936J" -> 'previous_id',
  "status": "complete",
  "refund_amount": 1999.0,
  "payment_methods": [
    {
      "apportioned_amount": 1999.0,
      "payment_mode": "Prepaid_Payments"
    }
  ]
}

"""


def process_IB(payment_method_list, previous_shipment, shipment_id, ref_refund_amount):
    paylaod_p = {
        "refund_source_id": shipment_id,
        "shipment_id": previous_shipment,
        "status": "complete",
        "refund_amount": ref_refund_amount,
        "payment_methods": payment_method_list
    }

    print(paylaod_p)

    url = "https://ril-prod-sellow-vpce.ril.smebazaar.ooo/pvt/api/shipments/update_refund_status.json"

    headers = {
        "Testing": "e1e50803314fade1dd8a930cd45bf3ce",
        "Authorization": "Bearer 25927aa4892a8c786b89017749c28cf26e85940fa8642436b0623ae488f807ab",
        "Content-Type": "application/json"
    }

    return_id = str(shipment_id)

    payload = json.dumps(paylaod_p)


    response = requests.request("POST", url, headers=headers, data=payload)

    print(response.text)

    print(response.status_code)


    with open("refund_completed_logs_saved.csv", 'a') as file:
        content_files = csv.writer(file)
        content_files.writerow(
            [str(previous_shipment), str(shipment_id), payload, str(response.json()), str(response.status_code),
             str(datetime.now())])
        file.close()


    logger.debug(f"----------Sending request to IB against shipment_id: {shipment_id}------")
    logger.debug(f"return_id: {return_id}")
    logger.debug(f'Request to url: {url}')
    logger.debug(f'Request headers: {response.request.headers}')
    logger.debug(f'Request body: {response.request.body}')
    logger.debug(f'----------------Response got for {shipment_id} from IB -----------------')
    logger.debug(f'Response headers: {response.headers}')
    logger.debug(f'Response body: {response.content.decode()}')
    logger.debug(f'Response code: {response.status_code}')
    logger.debug(f'Response reason:  {response.reason}')
    logger.debug(f'-------------------------------------------------------------------------')


input_files = ['16917417611131104249J','16917418303581100153J','16917423587201109960J','16917416786521101573J','16276485494921828715J','16502854436921280000J','16540765130171721350J','16519383672951030737J','16502974096911991916J','16385611217181203980J','16344931328351890131J','16325745941001180545J','16492512342721079166J','16494173313531345969J','16423299669951918352J','16264505017621070523J','16549409926121241481J','16781756362041721385J','16434545629601452884J','16494365327781025780J','16508995341141059771J','16311938903671686483J','16526939406651692025J','16560699825711042364J','16564129171721572589J','16704206307341002461J','16381858478571915669J','16302079659191618369J','16502791701011996169J','16464074980691238572J','16502789467741973167J','16315997220981954041J','16464076420831201095J','16263557165671117567J','16564061774941564440J','16552158334791747502J']

for input_file in input_files:

    shipments_data = db.shipments.find(
        {
            'shipment_id': input_file
        }
    )

    payment_methods = []
    for data_info in shipments_data:
        previous_shipment = data_info['shipment']['previous_shipment_id'] if data_info['shipment']['previous_shipment_id'] else data_info['shipment_id']
        shipment_id = data_info['shipment_id']
        bags = data_info['bags']
        apportioned_amount_cod = {"Postpaid_Payments": 0}
        apportioned_amount_paid = {"Prepaid_Payments": 0}

        for bag in bags:
            for mode, amt in bag['prices']['pm_price_split'].items():
                payment_methods_code = ('COD', 'CoD')

                if mode in payment_methods_code:
                    apportioned_amount_cod = {
                        "Postpaid_Payments": apportioned_amount_cod["Postpaid_Payments"] + amt
                    }
                else:
                    apportioned_amount_paid = {
                        "Prepaid_Payments": apportioned_amount_paid["Prepaid_Payments"] + amt
                    }

        if apportioned_amount_cod['Postpaid_Payments'] != 0:
            payment_methods.append({
                "apportioned_amount": round(float(apportioned_amount_cod["Postpaid_Payments"]), 2),
                "payment_mode": "COD"
            })

        if apportioned_amount_paid['Prepaid_Payments'] != 0:
            payment_methods.append({
                "apportioned_amount": round(float(apportioned_amount_paid["Prepaid_Payments"]), 2),
                "payment_mode": "Prepaid_Payments"
            })

        refund_amount = round(float(apportioned_amount_cod["Postpaid_Payments"]), 2) + round(float(apportioned_amount_paid["Prepaid_Payments"]), 2)

    process_IB(
        payment_method_list=payment_methods, previous_shipment=previous_shipment,
        shipment_id=shipment_id, ref_refund_amount=refund_amount
    )

    time.sleep(0.1)