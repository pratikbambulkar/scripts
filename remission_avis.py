shipment_ids=['16802437127901162138J']
from models import *
session=Backend().get_session()
sss=session.query(ShipmentStatus).filter(ShipmentStatus.shipment_id.in_(shipment_ids)).all()
shipment_updated_time = time()
for ss in sss:
    if not ss.meta or not ss.meta.get("kafka_emission_status"):
        KafkaProducerHelper.emit_tasks(
            methods=["emit_shipment_status_event"],
            partition_key=ss.shipment_id,
            shipment_id=ss.shipment_id,
            shipment_status_id=ss.id,
            shipment_updated_time=shipment_updated_time,
            action="shipment_status_update",
            status=ss.status
        )