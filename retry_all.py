from connections import Backend
from models import Integration, Task, TaskEvents
import requests
import time

session = Backend().get_session()

def gen_batch(data, size: int):
    max_size = len(data)
    if size > max_size:
        size = max_size
    for index in range(0, max_size, size):
        yield data[index:index + size]


def new_hog_retry(uid):
    url = f"https://api.jioecomm.com/hogwarts/api/v1/manual-retry-api?uid={uid}"

    payload = {}
    headers = {}

    response = requests.request("GET", url, headers=headers, data=payload)

    print(response.text)
    time.sleep(0.2)

data_obj = session.query(Task.uid).filter(Task.integration_id == 5, Task.status == 'failed',
                                          Task.created_on > '2023-04-07 00:00:00').all()

uid_ids = [i for i in (x[0] for x in data_obj)]

if len(uid_ids) != 0:
    ids_lists = gen_batch(uid_ids, 5)

    for i in ids_lists:
        for j in i:
            new_hog_retry(j)
else:
    print("No uids")





