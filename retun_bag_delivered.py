import pip


def install(package):
    if hasattr(pip, 'main'):
        pip.main(['install', package])
    else:
        pip._internal.main(['install', package])


if __name__ == '__main__':
    install('pandas')
    install('csv')

import json
import csv
import requests
from datetime import datetime
import pandas as pd


def return_bag_delivered_api_push(awb_no, shipment_id, return_id):
    url = "https://ril-prod-sellow-vpce.ril.smebazaar.ooo/pvt/api/returns/update_status.json"

    payload = json.dumps({
        "status": "items_returned_to_seller",
        "return_id": return_id,
        "tracking_code": str(awb_no),
        "shipment_id": shipment_id,
        "waybill": [
            str(awb_no)
        ]
    })

    headers = {
        'Content-Type': 'application/json',
        'Testing': 'e1e50803314fade1dd8a930cd45bf3ce',
        'Authorization': 'Bearer 859e650013133ed39295d2b433212026f008764143baac3117c859e9243588eb'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    print(response.text)

    print(response.status_code)

    with open("return_bag_delivered_logs.csv", 'a') as file:
        content_files = csv.writer(file)
        content_files.writerow(
            [str(shipment_id), str(return_id), payload, str(response.json()), str(response.status_code),
             str(response.text),
             str(datetime.now())])
        file.close()


with open("return_bag_delivered_logs.csv", 'a') as cf:
    content_file = csv.writer(cf)
    content_file.writerow(
        ["shipment_id", "Return_id", "Payload", "Response_text", "Status code", "Response_text", "date_time"])
    cf.close()

return_bag_delivered_data = pd.read_csv('return_bag_delivered_data.csv')

for i, j, k in zip(return_bag_delivered_data['shipment_id'],
                      return_bag_delivered_data['awb_no'],
                      return_bag_delivered_data['return_id'],
                      ):
    return_bag_delivered_api_push(awb_no=j, shipment_id=i, return_id=k)

print('done')
