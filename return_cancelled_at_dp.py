import pip
import os
import time


def install(package):
    if hasattr(pip, 'main'):
        pip.main(['install', package])
    else:
        pip._internal.main(['install', package])


if __name__ == '__main__':
    install('pandas')
    install('csv')

import json
import csv
import requests
from datetime import datetime
import pandas as pd

# os.remove("cancelled_at_dp_logs.csv")
# os.remove("cancelled_at_dp_data.csv")


def return_cancelled_at_dp_api_push(awb_no, shipment_id, shipping_agent, return_id):
    url = "https://ril-prod-sellow-vpce.ril.smebazaar.ooo/pvt/api/returns/update_status.json"

    payload = json.dumps({

            "tracking_code": str(awb_no),
            "status": "pickup_failed",
            "shipping_agent_code": str(shipping_agent),
            "waybill": [
                str(awb_no)
            ],
            "return_id": return_id,
            "shipment_id": shipment_id

    })

    headers = {
        'Content-Type': 'application/json',
        'Testing': 'e1e50803314fade1dd8a930cd45bf3ce',
        'Authorization': 'Bearer 58c28bb595ce997b54856078931712d59a61bfe2a52dfe6e0f9cfa5319a7c290'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    print(response.text)

    print(response.status_code)

    time.sleep(2)

    with open("cancelled_at_dp_logs.csv", 'a') as file:
        content_files = csv.writer(file)
        content_files.writerow(
            [str(shipment_id), str(return_id), payload, str(response.json()), str(response.status_code),
             str(response.text),
             str(datetime.now())])
        file.close()


with open("cancelled_at_dp_logs.csv", 'a') as cf:
    content_file = csv.writer(cf)
    content_file.writerow(
        ["shipment_id", "Return_id", "Payload", "Response_text", "Status code", "Response_text", "date_time"])
    cf.close()

cancelled_at_dp_data = pd.read_csv('cancelled_at_dp_data.csv')

for i, j, k, l in zip(cancelled_at_dp_data['shipment_id'],
                      cancelled_at_dp_data['awb_no'],
                      cancelled_at_dp_data['return_id'],
                      cancelled_at_dp_data['shipping_agent']):

    return_cancelled_at_dp_api_push(awb_no=j, shipment_id=i, shipping_agent=l, return_id=k)

print('done')


