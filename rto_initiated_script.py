import csv
import pandas as pd
import ast
import json
import requests
from datetime import datetime

rto_payload = pd.read_csv('RTO_Payload.csv')

with open('RTO_logs.csv', 'a') as cf:
    file = csv.writer(cf)
    file.writerow(['Shipment_Id', 'Payload', 'Response', 'Date'])
    cf.close()


def rto_initiated(shipment_id, data):
    url = 'https://ril-prod-sellow-vpce.ril.smebazaar.ooo/pvt/api/returns/lsp_initiated_return.json'
    payload = json.dumps(data)
    # print(payload)
    headers = {
        'Testing': 'e1e50803314fade1dd8a930cd45bf3ce',
        'Authorization': '7ad33c73393ca221ed2f19376d4f104de740a57dff66e15350955417c7896080'  # get the latest bearer
        # token
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    print(response.text)

    print(response.status_code)

    with open('RTO_logs.csv', 'a') as xp:
        content_file = csv.writer(xp)
        content_file.writerow([str(shipment_id), payload, response.json(), str(datetime.now())])
        xp.close()


for i, j in zip(rto_payload['primary_identifier'], rto_payload['payload']):
    dictionary = ast.literal_eval(j)
    if dictionary['reason'] is None or dictionary['status'] is None:
        dictionary['reason'] = 'None'
        dictionary['status'] = 'None'
        rto_initiated(i, dictionary)
