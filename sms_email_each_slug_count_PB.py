from pymongo import MongoClient
from datetime import datetime
from collections import defaultdict
from pymongo import MongoClient, ASCENDING, DESCENDING
from datetime import datetime
from json import dumps
import os

uri = os.environ.get("MONGO_LOGS_READ_WRITE")

# Connect to MongoDB
client = MongoClient(uri)

# Select database and collection
db = client.logs


# # Set the date range
start_date = datetime(2023, 5, 1)
end_date = datetime(2023, 5, 2)

# Build the query
query = {
    'created_at': {'$gte': start_date, '$lte': end_date}
}

# Group and count by email_template
group = {
    '_id': '$sms.template',
    'count': {'$sum': 1}
}

# Execute the query
result = db.pointblank_comms_logging.aggregate([
    {'$match': query},
    {'$group': group}
])

# Print the results
for item in result:
    print(item)