import pandas as pd
import os

# Read the large Excel file
large_file_path = 'products.xlsx'
sheet_name = 'Sheet1'  # Replace with the actual sheet name if it's different
columns = ['Name','Item Code','Brand','Category','Description','Short Description','Reporting HSN Code','Country of Origin','Media','Multi Size','Gtin Type','Gtin Value','Seller Identifier','Meta','Size Meta','Size','Actual Price','Selling Price','Currency','Length (cm)','Width (cm)','Height (cm)','Product Dead Weight (gram)','Size Guide','Available','Highlights','Dependent Product','Variant Type','Variant Group ID','Variant Media','Trader Type','Trader Name','Trader Address','Track Inventory','Teaser Tag Name','No of Boxes','Manufacturing Time','Manufacturing Time Unit','Return Time Limit','Return Time Unit','Product Publishing Date','Tags','Net Quantity Value','Net Quantity Unit','Product Bundle','availabilityStatus','activeStatus','productType','Schedule','itemType','packageType','packSize','packSizeUnit','Dosage','dosageUnit','cartMinQty','cartMaxQty','productURL','searchKeys','inventroyLookup','refundStatus','coldStorage','dpcoStatus','foodLabel','eanCode','boxQty','unitSaleFlag','onlineSaleStatus','unitCount','procuredBy','popularity','brandOrGeneric','refillStatus','createdDate','modifiedDate','requestBy','isAllowPayment','catalogVisibility','searchVisibility','releaseTypeName','fssaiNo','marginPercentage','isActive','ptrPercentage','ptrPercentageType','minimumQty','maximumQty','defaultQty','minMargin','dpcoCeilingMrp','genericName','genericDosage','marketerName','marketerAddress','divisionName','importerName','importerAddress','cimsClassification','shelfLife','netQty','unitsInKit','vegOrNonVeg','seoTitle','s e o M e t a D e s c r i p t i o n','seoMetaKeywords','otxFlag','cartonQty','vendorNonReturnable','sapCode','dlFlag','truuthFlag','recommQty','cimssubcategoryname','CustomercareNo','CustomercareEmail','similarProducts','alternateProducts','boughtTogetherProducts']  # List of columns you want to keep

df_large = pd.read_excel(large_file_path, sheet_name=sheet_name, usecols=columns)

# Replace NaN values with an empty string in all columns
df_large = df_large.fillna("")

# Define the maximum file size (2 MB in bytes) and maximum rows per file
max_file_size = 2 * 1024 * 1024  # 2 MB in bytes
max_rows_per_file = 5000

# Create a directory to store the smaller files
output_directory = 'output_files'
os.makedirs(output_directory, exist_ok=True)

# Initialize variables
current_file_index = 1
current_rows = []
current_size = 0

# Split the data into smaller files
for index, row in df_large.iterrows():
    # Convert the row to CSV format and calculate its size
    row_csv = ','.join(['"' + str(cell) + '"' for cell in row]) + '\n'
    row_size = len(row_csv.encode('utf-8'))

    # Check if adding the current row exceeds the file size limit
    if current_size + row_size > max_file_size or len(current_rows) >= max_rows_per_file:
        # Write the current rows to a new CSV file
        output_file_path = os.path.join(output_directory, f'smaller_file_{current_file_index}.csv')
        with open(output_file_path, 'w', newline='', encoding='utf-8') as output_file:
            output_file.write(','.join(columns) + '\n')
            output_file.writelines(current_rows)

        # Reset variables for the next file
        current_file_index += 1
        current_rows = []
        current_size = 0

    # Add the current row to the current set
    current_rows.append(row_csv)
    current_size += row_size

# Write the last set of rows to a file
if current_rows:
    output_file_path = os.path.join(output_directory, f'smaller_file_{current_file_index}.csv')
    with open(output_file_path, 'w', newline='', encoding='utf-8') as output_file:
        output_file.write(','.join(columns) + '\n')
        output_file.writelines(current_rows)

print("Splitting complete.")
