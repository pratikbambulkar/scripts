import pymongo
import csv
import pandas as pd

# connect to the mongo-client
client = pymongo.MongoClient("mongodb://firebolt_readWrite:firebolt_readWrite!2018@10.0.81.164:27017/firebolt")

# get the database
database = client['firebolt']

entity_collection = database.get_collection("entity")
store_collection = database.get_collection("store")

cursor = store_collection.find({"_custom_json.fynd_store_type": {"$in": ["3P"]}})

entity_data = pd.DataFrame(list(cursor))

entity_data['Company_name'] = entity_data['company_details'].apply(lambda x: x['company_name'])
entity_data['Company_id'] = entity_data['company_details'].apply(lambda x: x['company_id'])
entity_data['verticals'] = entity_data['_custom_json'].apply(lambda x: x['verticals'])
entity_data[['code', 'Company_name', 'Company_id', 'verticals']].to_csv('test_data.csv', index=False)

